package de.csbme.schulplaner.server.command;

import de.csbme.schulplaner.lib.user.UserGroup;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.util.command.Command;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class UserGroupCommand extends Command {

    private Main main;

    public UserGroupCommand( Main main ) {
        super( "usergroup" );
        this.main = main;
    }

    @Override
    public void execute( String label, String[] args ) {
        if ( args.length != 2 ) {
            this.main.getLogger().info( "Verwendung: usergroup <Email|UserId> <Gruppenname>" );
            return;
        }
        Main.POOL.execute( () -> {
            int userId;
            try {
                userId = Integer.parseInt( args[0] );
            } catch ( NumberFormatException e ) {
                try {
                    Integer id = this.main.getUserRepository().getUserIdFromMail( args[0] ).get();
                    if ( id == null ) {
                        this.main.getLogger().info( "Es wurde kein User mit dieser Email/ID gefunden!" );
                        return;
                    }
                    userId = id;
                } catch ( InterruptedException | ExecutionException ex ) {
                    this.main.getLogger().error( "Es ist ein Fehler aufgetreten!", ex );
                    return;
                }
            }
            Optional<UserGroup> optionalUserGroup = Arrays.stream( UserGroup.values() ).filter( u -> u.name().equalsIgnoreCase( args[1] ) ).findFirst();
            if ( !optionalUserGroup.isPresent() ) {
                this.main.getLogger().info( "Es wurde keine Gruppe mit diesem Namen gefunden!" );
                return;
            }
            try {
                Integer changed = this.main.getUserRepository().updateGroup( userId, optionalUserGroup.get() ).get();
                if ( changed != null && changed == 1 ) {
                    this.main.getLogger().info( "Die Gruppe '" + args[1] + "' wurde dem User mit der ID " + userId + " gesetzt." );
                    return;
                }
                this.main.getLogger().info( "Es wurden keine Daten verändert!" );
            } catch ( InterruptedException | ExecutionException e ) {
                this.main.getLogger().error( "Es ist ein Fehler aufgetreten!", e );
            }
        } );
    }
}
