package de.csbme.schulplaner.server.network;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.error.HTTPCodes;
import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.network.packets.ErrorPacket;
import de.csbme.schulplaner.lib.network.packets.changelog.ChangelogRequestPacket;
import de.csbme.schulplaner.lib.network.packets.changelog.ChangelogResponsePacket;
import de.csbme.schulplaner.lib.network.packets.grade.GradeDeletePacket;
import de.csbme.schulplaner.lib.network.packets.grade.GradeRequestPacket;
import de.csbme.schulplaner.lib.network.packets.grade.GradeUpdatePacket;
import de.csbme.schulplaner.lib.network.packets.note.NoteDeletePacket;
import de.csbme.schulplaner.lib.network.packets.note.NoteUpdatePacket;
import de.csbme.schulplaner.lib.network.packets.quiz.*;
import de.csbme.schulplaner.lib.network.packets.schoolclass.*;
import de.csbme.schulplaner.lib.network.packets.subject.*;
import de.csbme.schulplaner.lib.network.packets.timetable.TimetableDeletePacket;
import de.csbme.schulplaner.lib.network.packets.timetable.TimetableRefreshPacket;
import de.csbme.schulplaner.lib.network.packets.timetable.TimetableRequestPacket;
import de.csbme.schulplaner.lib.network.packets.timetable.TimetableUpdatePacket;
import de.csbme.schulplaner.lib.network.packets.user.*;
import de.csbme.schulplaner.lib.object.Class;
import de.csbme.schulplaner.lib.object.*;
import de.csbme.schulplaner.lib.user.Settings;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.user.User;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

public class ServerHandler extends SimpleChannelInboundHandler<Object> {

    private final Main main;

    ServerHandler(Main main) {
        this.main = main;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object object) throws Exception {
        try {
            if (!(object instanceof Packet)) {
                return;
            }
            Channel channel = channelHandlerContext.channel();
            Packet packet = (Packet) object;

            // Packet for user timeout
            if (packet instanceof UserAlivePacket) {
                UserAlivePacket userAlivePacket = (UserAlivePacket) packet;

                // Get user from name
                Optional<User> optionalUser = this.main.getUserManager().getUser(userAlivePacket.getId());

                // Run only if user exists
                optionalUser.ifPresent(user -> {
                    // Update timeout
                    user.updateTimeout();

                    // Check if the channel are equal
                    if (!user.getChannel().equals(channel)) {
                        // Replace channel
                        user.setChannel(channel);
                    }
                });
                return;
            }

            // Packet for user registration
            if (packet instanceof UserRegisterPacket) {
                UserRegisterPacket userRegisterPacket = (UserRegisterPacket) packet;

                // Check if mail exists
                Futures.addCallback(this.main.getUserRepository().existsMail(userRegisterPacket.getMailAddress()), new FutureCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean exists) {
                        if (exists) {
                            // Send error to Client
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_409, "Diese E-Mail wird bereits verwendet!"));
                            return;
                        }
                        // E-Mail available. Insert into database
                        ServerHandler.this.main.getUserManager().registerUser(userRegisterPacket.getMailAddress(), userRegisterPacket.getPassword(), channel);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        // Send error to Client
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_503, "Fehler bei der E-Mail Abfrage! Versuche es später erneut."));
                        ServerHandler.this.main.getLogger().error("Error while checking mail!", (Exception) throwable);
                    }
                }, MoreExecutors.directExecutor());
                return;
            }

            // Packet for user login
            if (packet instanceof UserLoginPacket) {
                UserLoginPacket userLoginPacket = (UserLoginPacket) packet;

                // Check if password correct
                Futures.addCallback(this.main.getUserRepository().isPasswordCorrect(userLoginPacket.getMailAddress(), userLoginPacket.getPassword()), new FutureCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean passwordCorrect) {
                        if (!passwordCorrect) {
                            // Send error to client
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Die Email oder das Passwort ist falsch!"));
                            // TODO send info to mail?
                            return;
                        }

                        // Password correct. Log User in
                        try {
                            Integer userId = ServerHandler.this.main.getUserRepository().getUserIdFromMail(userLoginPacket.getMailAddress()).get();
                            if (userId == null) {
                                // Send error to client
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_503, "User-ID konnte nicht gefunden werden! Wende dich an einen Admin!"));
                                return;
                            }

                            if (ServerHandler.this.main.getUserManager().getUser(userId).isPresent()) {
                                // Send error to client
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Du bist bereits eingeloggt!"));
                                return;
                            }

                            ServerHandler.this.main.getUserManager().loginUser(userId, channel);
                        } catch (InterruptedException | ExecutionException e) {
                            // Send error to client
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_503, "Fehler beim abfragen der User-ID!"));
                            ServerHandler.this.main.getLogger().error("Error while searching for User-ID!", e);
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        // Send error to client
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_503, "Fehler beim einloggen!"));
                        ServerHandler.this.main.getLogger().error("Error while login in!", (Exception) throwable);
                    }
                }, MoreExecutors.directExecutor());
                return;
            }

            // Packet for update note
            if (packet instanceof NoteUpdatePacket) {
                NoteUpdatePacket noteUpdatePacket = (NoteUpdatePacket) packet;
                Note note = noteUpdatePacket.getNote();

                Main.getInstance().getUserManager().getUser(noteUpdatePacket.getUserId()).ifPresent(user -> {
                    ListenableFuture<Integer> future;
                    final boolean inserted;
                    if (note.getNoteId() == -1) {
                        // Insert
                        future = Main.getInstance().getNoteRepository().insertNote(Note.OwnerType.USER, user.getId(), note);
                        inserted = true;
                    } else {
                        // Update
                        future = Main.getInstance().getNoteRepository().updateNote(note);
                        inserted = false;
                    }
                    Futures.addCallback(future, new FutureCallback<Integer>() {
                        @Override
                        public void onSuccess(Integer result) {
                            if (!inserted && result != 1) {
                                Main.getInstance().getLogger().error("Netty/User: Error while updating note!");
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                return;
                            }
                            Futures.addCallback(Main.getInstance().getNotificationRepository().updateNotification(user.getId(), inserted ? result : note.getNoteId(), note.getExpireDate()), new FutureCallback<Integer>() {
                                @Override
                                public void onSuccess(Integer result) {
                                    user.sendNotesToClient();
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    Main.getInstance().getLogger().error("Netty/User: Error while inserting note!", (Exception) t);
                                    channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                }
                            }, MoreExecutors.directExecutor());
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Main.getInstance().getLogger().error("Netty/User: Error while inserting note!", (Exception) t);
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        }
                    }, MoreExecutors.directExecutor());
                });
                return;
            }

            // Packet for deleting note
            if (packet instanceof NoteDeletePacket) {
                NoteDeletePacket noteDeletePacket = (NoteDeletePacket) packet;

                Main.getInstance().getUserManager().getUser(noteDeletePacket.getUserId()).ifPresent(user ->
                        Futures.addCallback(Main.getInstance().getNoteRepository().deleteNote(noteDeletePacket.getNoteId()), new FutureCallback<Integer>() {
                            @Override
                            public void onSuccess(Integer result) {
                                if (result != 1) {
                                    Main.getInstance().getLogger().error("Netty/Note: Can't delete note with id " + noteDeletePacket.getNoteId());
                                    channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                    return;
                                }
                                Futures.addCallback(Main.getInstance().getNotificationRepository().deleteNotification(user.getId(), noteDeletePacket.getNoteId()), new FutureCallback<Integer>() {
                                    @Override
                                    public void onSuccess(Integer result) {
                                        user.sendNotesToClient();
                                    }

                                    @Override
                                    public void onFailure(Throwable t) {
                                        Main.getInstance().getLogger().error("Netty/Note: Can't delete note with id " + noteDeletePacket.getNoteId(), (Exception) t);
                                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                    }
                                }, MoreExecutors.directExecutor());
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                Main.getInstance().getLogger().error("Netty/Note: Can't delete note with id " + noteDeletePacket.getNoteId(), (Exception) t);
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            }
                        }, MoreExecutors.directExecutor()));
                return;
            }

            // Packet for requesting grades
            if (packet instanceof GradeRequestPacket) {
                GradeRequestPacket gradeRequestPacket = (GradeRequestPacket) packet;

                Main.getInstance().getUserManager().getUser(gradeRequestPacket.getUserId()).ifPresent(User::sendGradesToClient);
                return;
            }

            // Packet for update grade
            if (packet instanceof GradeUpdatePacket) {
                GradeUpdatePacket gradeUpdatePacket = (GradeUpdatePacket) packet;
                Grade grade = gradeUpdatePacket.getGrade();

                Main.getInstance().getUserManager().getUser(gradeUpdatePacket.getUserId()).ifPresent(user -> {
                    ListenableFuture<Integer> future;
                    if (grade.getGradeId() == -1) {
                        // Insert
                        future = Main.getInstance().getGradeRepository().insertGrade(user.getId(), grade);
                    } else {
                        // Update
                        future = Main.getInstance().getGradeRepository().updateGrade(grade);
                    }
                    Futures.addCallback(future, new FutureCallback<Integer>() {
                        @Override
                        public void onSuccess(Integer result) {
                            if (result != 1) {
                                Main.getInstance().getLogger().error("Netty/User: Error while inserting grade!");
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                return;
                            }
                            user.sendGradesToClient();
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Main.getInstance().getLogger().error("Netty/User: Error while inserting grade!", (Exception) t);
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        }
                    }, MoreExecutors.directExecutor());
                });
                return;
            }

            // Packet for deleting grade
            if (packet instanceof GradeDeletePacket) {
                GradeDeletePacket gradeDeletePacket = (GradeDeletePacket) packet;

                Main.getInstance().getUserManager().getUser(gradeDeletePacket.getUserId()).ifPresent(user -> {
                    AtomicInteger atomicInteger = new AtomicInteger(0);
                    final int maxDeletes = gradeDeletePacket.getGradeIds().length;

                    Arrays.stream(gradeDeletePacket.getGradeIds()).forEach(i -> Futures.addCallback(Main.getInstance().getGradeRepository().deleteGrade(i), new FutureCallback<Integer>() {
                        @Override
                        public void onSuccess(Integer result) {
                            if (result != 1) {
                                Main.getInstance().getLogger().error("Netty/Note: Can't delete grade with id " + i);
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                return;
                            }
                            if (atomicInteger.incrementAndGet() == maxDeletes) {
                                user.sendGradesToClient();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Main.getInstance().getLogger().error("Netty/Note: Can't delete grade with id " + i, (Exception) t);
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        }
                    }, MoreExecutors.directExecutor()));
                });
                return;
            }

            // Packet for user logout
            if (packet instanceof UserLogoutPacket) {
                UserLogoutPacket userLogoutPacket = (UserLogoutPacket) packet;

                Main.getInstance().getUserManager().getUser(userLogoutPacket.getUserId()).ifPresent(user -> {
                    Main.getInstance().getUserManager().logOutUser(user);
                    channel.writeAndFlush(userLogoutPacket);
                });
                return;
            }

            // Packet for updating mail
            if (packet instanceof UserMailAddressUpdatePacket) {
                UserMailAddressUpdatePacket userMailAddressUpdatePacket = (UserMailAddressUpdatePacket) packet;

                Futures.addCallback(Main.getInstance().getUserRepository().existsMail(userMailAddressUpdatePacket.getMailAddress()), new FutureCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean exists) {
                        if (exists) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Diese E-Mail existiert bereits!"));
                            return;
                        }
                        Main.getInstance().getUserManager().getUser(userMailAddressUpdatePacket.getUserId()).ifPresent(user -> {
                            Main.getInstance().getLogger().info("Netty/ChangeMail: User '" + user.getMailAddress() + " updated mail to '" + userMailAddressUpdatePacket.getMailAddress() + "'");
                            Main.getInstance().getUserRepository().updateMailAddress(user.getId(), userMailAddressUpdatePacket.getMailAddress());
                            user.setMailAddress(userMailAddressUpdatePacket.getMailAddress());
                            channel.writeAndFlush(new UserMailAddressUpdatedPacket(user.getId(), userMailAddressUpdatePacket.getMailAddress()));
                        });
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        Main.getInstance().getLogger().error("Netty/ChangeMail: Error while changing mail!", (Exception) t);
                    }
                }, MoreExecutors.directExecutor());
                return;
            }

            // Packet for updating password
            if (packet instanceof UserPasswordUpdatePacket) {
                UserPasswordUpdatePacket userPasswordUpdatePacket = (UserPasswordUpdatePacket) packet;

                Main.getInstance().getUserManager().getUser(userPasswordUpdatePacket.getUserId()).ifPresent(user ->
                        Futures.addCallback(Main.getInstance().getUserRepository().isPasswordCorrect(user.getMailAddress(),
                                userPasswordUpdatePacket.getOldPassword()), new FutureCallback<Boolean>() {
                            @Override
                            public void onSuccess(Boolean correct) {
                                if (!correct) {
                                    channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_403, "Das alte Passwort ist falsch!"));
                                    return;
                                }
                                Main.getInstance().getUserRepository().updatePassword(user.getId(), userPasswordUpdatePacket.getNewPassword());
                                Main.getInstance().getLogger().info("Netty/ChangeMail: User '" + user.getMailAddress() + "' changed password!");
                                channel.writeAndFlush(new UserPasswordUpdatedPacket(user.getId()));
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                Main.getInstance().getLogger().error("Netty/ChangeMail: Error while changing mail!", (Exception) t);
                            }
                        }, MoreExecutors.directExecutor()));
                return;
            }

            // Packet for changelog request
            if (packet instanceof ChangelogRequestPacket) {
                Futures.addCallback(Main.getInstance().getChangelogRepository().getChangelogs(), new FutureCallback<List<Changelog>>() {
                    @Override
                    public void onSuccess(List<Changelog> result) {
                        channel.writeAndFlush(new ChangelogResponsePacket(result));
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        Main.getInstance().getLogger().error("Netty/Changelog: Error while requesting changelog!", (Exception) t);
                    }
                }, MoreExecutors.directExecutor());
                return;
            }

            // Packet for update settings
            if (packet instanceof UserManipulateSettingPacket) {
                UserManipulateSettingPacket userManipulateSettingPacket = (UserManipulateSettingPacket) packet;

                Main.getInstance().getSettingRepository().manipulateSetting(userManipulateSettingPacket.getUserId(),
                        Settings.Key.values()[userManipulateSettingPacket.getSettingsKey()],
                        userManipulateSettingPacket.getSettingsValue());
                return;
            }

            // Packet for update user name
            if (packet instanceof UserNameUpdatePacket) {
                UserNameUpdatePacket userNameUpdatePacket = (UserNameUpdatePacket) packet;

                Main.getInstance().getUserRepository().updateName(userNameUpdatePacket.getUserId(), userNameUpdatePacket.getUsername());
                Main.getInstance().getUserManager().getUser(userNameUpdatePacket.getUserId()).ifPresent(user -> {
                    user.setUsername(userNameUpdatePacket.getUsername());
                    Main.getInstance().getLogger().info("Netty/NameChange: User '" + user.getMailAddress() + "' changed username to '" + user.getUsername() + "'");
                });
                channel.writeAndFlush(userNameUpdatePacket);
                return;
            }

            // Packet for class create
            if (packet instanceof ClassCreatePacket) {
                ClassCreatePacket classCreatePacket = (ClassCreatePacket) packet;

                Main.getInstance().getUserManager().getUser(classCreatePacket.getUserId()).ifPresent(user -> {
                    ListenableFuture<Integer> futureInsert = Main.getInstance().getClassRepository().insertClass(
                            classCreatePacket.getName(), classCreatePacket.getSchoolName(), user.getId());
                    Futures.addCallback(futureInsert, new FutureCallback<Integer>() {
                        @Override
                        public void onSuccess(Integer result) {
                            user.setClassId(result);
                            Main.getInstance().getUserRepository().updateClass(user.getId(), result);
                            try {
                                channel.writeAndFlush(new ClassCreatedPacket(user.getId(),
                                        Main.getInstance().getClassRepository().getClassById(result).get()));
                            } catch (InterruptedException | ExecutionException e) {
                                e.printStackTrace();
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Deine Klasse konnte nicht abgerufen!"));
                                Main.getInstance().getLogger().error("Netty/ClassCreate: Error while fetching class!");
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            Main.getInstance().getLogger().error("Netty/ClassCreate: Error while creating class!", (Exception) t);
                        }
                    }, MoreExecutors.directExecutor());
                });
                return;
            }

            // Packet for joining class
            if (packet instanceof ClassJoinPacket) {
                ClassJoinPacket classJoinPacket = (ClassJoinPacket) packet;

                Main.getInstance().getUserManager().getUser(classJoinPacket.getUserId()).ifPresent(user -> {
                    Futures.addCallback(Main.getInstance().getClassRepository().existsClassCode(classJoinPacket.getClassCode()), new FutureCallback<Boolean>() {
                        @Override
                        public void onSuccess(Boolean exists) {
                            if (!exists) {
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Dieser Klassencode wurde nicht gefunden!"));
                                return;
                            }
                            Futures.addCallback(Main.getInstance().getClassRepository().getClassByCode(classJoinPacket.getClassCode()), new FutureCallback<Class>() {
                                @Override
                                public void onSuccess(Class classInfo) {
                                    user.setClassId(classInfo.getClassId());
                                    Main.getInstance().getUserRepository().updateClass(user.getId(), classInfo.getClassId());
                                    channel.writeAndFlush(new ClassJoinedPacket(user.getId(), classInfo));
                                    try {
                                        channel.writeAndFlush(new SubjectRefreshPacket(
                                                Main.getInstance().getSubjectRepository().getSubjects(classInfo.getClassId()).get()));
                                    } catch (InterruptedException | ExecutionException e) {
                                        e.printStackTrace();
                                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                        Main.getInstance().getLogger().error("Netty/ClassJoin: Error while joining class!");
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                    Main.getInstance().getLogger().error("Netty/ClassJoin: Error while joining class!", (Exception) t);
                                }
                            }, MoreExecutors.directExecutor());
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            Main.getInstance().getLogger().error("Netty/ClassJoin: Error while joining class!", (Exception) t);
                        }
                    }, MoreExecutors.directExecutor());
                });
                return;
            }

            // Packet for leaving class
            if (packet instanceof ClassLeavePacket) {
                ClassLeavePacket classLeavePacket = (ClassLeavePacket) packet;

                Main.getInstance().getUserManager().getUser(classLeavePacket.getUserId()).ifPresent(user -> {
                    user.setClassId(-1);
                    Main.getInstance().getUserRepository().updateClass(user.getId(), -1);
                    channel.writeAndFlush(new ClassLeftPacket(user.getId()));
                });
                return;
            }

            // Packet for requesting member size
            if (packet instanceof ClassMemberSizeRequestPacket) {
                ClassMemberSizeRequestPacket classMemberSizeRequestPacket = (ClassMemberSizeRequestPacket) packet;

                Futures.addCallback(Main.getInstance().getClassRepository().getClassMemberSize(classMemberSizeRequestPacket.getClassId()), new FutureCallback<Long>() {
                    @Override
                    public void onSuccess(Long result) {
                        channel.writeAndFlush(new ClassMemberSizeResponsePacket(result));
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        Main.getInstance().getLogger().error("Netty/ClassMember: Error while fetching class member size!", (Exception) t);
                    }
                }, MoreExecutors.directExecutor());
                return;
            }

            // Packet for creating subject
            if (packet instanceof SubjectCreatePacket) {
                SubjectCreatePacket subjectCreatePacket = (SubjectCreatePacket) packet;

                Main.getInstance().getUserManager().getUser(subjectCreatePacket.getUserId()).ifPresent(user -> Futures.addCallback(Main.getInstance().getClassRepository().isClassOwner(user.getClassId(), user.getId()), new FutureCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean isOwner) {
                        if (!isOwner) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_401, "Nur der Ersteller der Klasse kann Fächer hinzufügen!"));
                            return;
                        }
                        try {
                            Main.getInstance().getSubjectRepository().insertSubject(subjectCreatePacket.getClassId(),
                                    subjectCreatePacket.getSubjectName(), subjectCreatePacket.getSubjectShortName()).get();

                            Main.getInstance().getSubjectRepository().sendSubjectsToClassClients(subjectCreatePacket.getClassId(),
                                    Main.getInstance().getClassRepository().getClassUsers(subjectCreatePacket.getClassId()).get());
                            channel.writeAndFlush(new SubjectCreatedPacket(user.getId()));
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        Main.getInstance().getLogger().error("Netty/ClassMember: Error while insert subject!", (Exception) t);
                    }
                }, MoreExecutors.directExecutor()));
                return;
            }

            // Packet for subject deleting
            if (packet instanceof SubjectDeletePacket) {
                SubjectDeletePacket subjectDeletePacket = (SubjectDeletePacket) packet;

                Main.getInstance().getUserManager().getUser(subjectDeletePacket.getUserId()).ifPresent(user -> Futures.addCallback(Main.getInstance().getClassRepository().isClassOwner(user.getClassId(), user.getId()), new FutureCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean owner) {
                        if (!owner) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_401, "Nur der Ersteller der Klasse kann Fächer löschen!"));
                            return;
                        }
                        try {
                            Main.getInstance().getSubjectRepository().deleteSubject(subjectDeletePacket.getSubjectId()).get();

                            Main.getInstance().getSubjectRepository().sendSubjectsToClassClients(user.getClassId(),
                                    Main.getInstance().getClassRepository().getClassUsers(user.getClassId()).get());
                            user.sendNotesToClient();
                            user.sendGradesToClient();

                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    channel.writeAndFlush(new SubjectDeletedPacket(user.getId()));
                                }
                            }, 200L);
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        Main.getInstance().getLogger().error("Netty/ClassMember: Error while insert subject!", (Exception) t);
                    }
                }, MoreExecutors.directExecutor()));
                return;
            }

            // Packet for updating subject
            if (packet instanceof SubjectUpdatePacket) {
                SubjectUpdatePacket subjectUpdatePacket = (SubjectUpdatePacket) packet;

                Main.getInstance().getUserManager().getUser(subjectUpdatePacket.getUserId()).ifPresent(user -> Futures.addCallback(Main.getInstance().getClassRepository().isClassOwner(user.getClassId(), user.getId()), new FutureCallback<Boolean>() {
                    @Override
                    public void onSuccess(Boolean owner) {
                        if (!owner) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_401, "Nur der Ersteller der Klasse kann Fächer bearbeiten!"));
                            return;
                        }
                        try {
                            Main.getInstance().getSubjectRepository().updateSubject(subjectUpdatePacket.getSubjectId(),
                                    subjectUpdatePacket.getSubjectName(), subjectUpdatePacket.getSubjectShortName()).get();

                            Main.getInstance().getSubjectRepository().sendSubjectsToClassClients(user.getClassId(),
                                    Main.getInstance().getClassRepository().getClassUsers(user.getClassId()).get());

                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    channel.writeAndFlush(new SubjectUpdatedPacket(user.getId()));
                                }
                            }, 200L);
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        Main.getInstance().getLogger().error("Netty/ClassMember: Error while insert subject!", (Exception) t);
                    }
                }, MoreExecutors.directExecutor()));
                return;
            }

            // Packet for timetable requesting
            if (packet instanceof TimetableRequestPacket) {
                TimetableRequestPacket timetableRequestPacket = (TimetableRequestPacket) packet;

                Main.getInstance().getUserManager().getUser(timetableRequestPacket.getUserId()).ifPresent(user -> {
                    if (user.getClassId() == -1) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_404, "Es wurde für dich kein Stundenplan gefunden, da du in keiner Klasse bist!"));
                        return;
                    }
                    Futures.addCallback(Main.getInstance().getTimetableRepository().getTimetable(user.getClassId()), new FutureCallback<Timetable>() {
                        @Override
                        public void onSuccess(Timetable result) {
                            channel.writeAndFlush(new TimetableRefreshPacket(user.getId(), result));
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            Main.getInstance().getLogger().error("Netty/Timetable: Error while fetching timetable!", (Exception) t);
                        }
                    }, MoreExecutors.directExecutor());
                });
                return;
            }

            // Packet for receiving timetable update
            if (packet instanceof TimetableUpdatePacket) {
                TimetableUpdatePacket timetableUpdatePacket = (TimetableUpdatePacket) packet;

                Main.getInstance().getUserManager().getUser(timetableUpdatePacket.getUserId()).ifPresent(user -> {
                    if (user.getClassId() == -1) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_404, "Du bist in keiner Klasse!"));
                        return;
                    }
                    Timetable.SchoolHour[] hours = timetableUpdatePacket.getSchoolHours();
                    final int maxSize = hours.length;
                    AtomicInteger currentSize = new AtomicInteger(0);

                    Futures.addCallback(Main.getInstance().getClassRepository().isClassOwner(user.getClassId(), user.getId()), new FutureCallback<Boolean>() {
                        @Override
                        public void onSuccess(Boolean owner) {
                            if (!owner) {
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_401, "Nur der Ersteller der Klasse kann den Stundenplan ändern!"));
                                return;
                            }
                            Arrays.stream(hours).forEach(h -> {
                                ListenableFuture<Integer> future;
                                if (h.getSchoolHourId() == -1) {
                                    // Insert
                                    future = Main.getInstance().getTimetableRepository().insertSchoolHour(user.getClassId(), h);
                                } else {
                                    // Update
                                    future = Main.getInstance().getTimetableRepository().updateSchoolHour(user.getClassId(), h);
                                }
                                Futures.addCallback(future, new FutureCallback<Integer>() {
                                    @Override
                                    public void onSuccess(Integer result) {
                                        if (result != 1) {
                                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                            Main.getInstance().getLogger().error("Netty/Timetable: Error while updating timetable!");
                                            return;
                                        }
                                        if (currentSize.incrementAndGet() == maxSize) {
                                            try {
                                                channel.writeAndFlush(new TimetableRefreshPacket(user.getId(), Main.getInstance().getTimetableRepository().getTimetable(user.getClassId()).get()));
                                            } catch (InterruptedException | ExecutionException e) {
                                                e.printStackTrace();
                                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                                Main.getInstance().getLogger().error("Netty/Timetable: Error while updating timetable!", e);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Throwable t) {
                                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                        Main.getInstance().getLogger().error("Netty/Timetable: Error while updating timetable!", (Exception) t);
                                    }
                                }, MoreExecutors.directExecutor());
                            });
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            Main.getInstance().getLogger().error("Netty/Timetable: Error while updating timetable!", (Exception) t);
                        }
                    }, MoreExecutors.directExecutor());
                });
                return;
            }

            // Packet for deleting timetable school hour
            if (packet instanceof TimetableDeletePacket) {
                TimetableDeletePacket timetableDeletePacket = (TimetableDeletePacket) packet;

                Main.getInstance().getUserManager().getUser(timetableDeletePacket.getUserId()).ifPresent(user -> {
                    if (user.getClassId() == -1) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_404, "Du bist in keiner Klasse!"));
                        return;
                    }
                    Futures.addCallback(Main.getInstance().getClassRepository().isClassOwner(user.getClassId(), user.getId()), new FutureCallback<Boolean>() {
                        @Override
                        public void onSuccess(Boolean owner) {
                            if (!owner) {
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_401, "Nur der Ersteller der Klasse kann den Stundenplan ändern!"));
                                return;
                            }
                            Futures.addCallback(Main.getInstance().getTimetableRepository().deleteSchoolHour(timetableDeletePacket.getSchoolHourId()), new FutureCallback<Integer>() {
                                @Override
                                public void onSuccess(Integer result) {
                                    if (result != 1) {
                                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                        Main.getInstance().getLogger().error("Netty/Timetable: Error while deleting timetable!");
                                        return;
                                    }
                                    try {
                                        channel.writeAndFlush(new TimetableRefreshPacket(user.getId(), Main.getInstance().getTimetableRepository().getTimetable(user.getClassId()).get()));
                                    } catch (InterruptedException | ExecutionException e) {
                                        e.printStackTrace();
                                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                        Main.getInstance().getLogger().error("Netty/Timetable: Error while deleting timetable!", e);
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                    Main.getInstance().getLogger().error("Netty/Timetable: Error while deleting timetable!", (Exception) t);
                                }
                            }, MoreExecutors.directExecutor());
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            Main.getInstance().getLogger().error("Netty/Timetable: Error while deleting timetable!", (Exception) t);
                        }
                    }, MoreExecutors.directExecutor());
                });
                return;
            }

            // Packet requesting quizzes
            if (packet instanceof QuizRequestPacket) {
                QuizRequestPacket quizRequestPacket = (QuizRequestPacket) packet;
                Futures.addCallback(Main.getInstance().getQuizRepository().getOwnAndPreSetsQuizzes(quizRequestPacket.getUserId()), new FutureCallback<List<Quiz>>() {
                    @Override
                    public void onSuccess(List<Quiz> result) {
                        channel.writeAndFlush(new QuizResponsePacket(result));
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        Main.getInstance().getLogger().error("Netty/Quiz: Error while fetching quizzes!", (Exception) t);
                    }
                }, MoreExecutors.directExecutor());
                return;
            }

            // Packet for creating/updating quiz
            if (packet instanceof QuizCreateUpdatePacket) {
                QuizCreateUpdatePacket quizCreateUpdatePacket = (QuizCreateUpdatePacket) packet;
                Quiz quiz = quizCreateUpdatePacket.getQuiz();

                Main.getInstance().getUserManager().getUser(quizCreateUpdatePacket.getUserId()).ifPresent(user -> {
                    ListenableFuture<Integer> future;
                    if (quiz.getQuizId() == -1) {
                        // Insert
                        future = Main.getInstance().getQuizRepository().insertQuiz(quiz);
                    } else {
                        // Update
                        future = Main.getInstance().getQuizRepository().updateQuiz(quiz);
                    }
                    Futures.addCallback(future, new FutureCallback<Integer>() {
                        @Override
                        public void onSuccess(Integer result) {
                            if (result != 1) {
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                Main.getInstance().getLogger().error("Netty/Quiz: Error while updating quiz!");
                                return;
                            }
                            try {
                                channel.writeAndFlush(new QuizResponsePacket(Main.getInstance().getQuizRepository().getOwnAndPreSetsQuizzes(user.getId()).get()));
                            } catch (InterruptedException | ExecutionException e) {
                                e.printStackTrace();
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                                Main.getInstance().getLogger().error("Netty/Quiz: Error while updating quiz!", e);
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            Main.getInstance().getLogger().error("Netty/Quiz: Error while updating quiz!", (Exception) t);
                        }
                    }, MoreExecutors.directExecutor());
                });
                return;
            }

            // Packet for deleting quiz
            if (packet instanceof QuizDeletePacket) {
                QuizDeletePacket quizDeletePacket = (QuizDeletePacket) packet;

                Futures.addCallback(Main.getInstance().getQuizRepository().deleteQuiz(quizDeletePacket.getQuizId()), new FutureCallback<Integer>() {
                    @Override
                    public void onSuccess(Integer result) {
                        if (result != 1) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            Main.getInstance().getLogger().error("Netty/Quiz: Error while deleting quiz!");
                            return;
                        }
                        try {
                            channel.writeAndFlush(new QuizResponsePacket(Main.getInstance().getQuizRepository().getOwnAndPreSetsQuizzes(quizDeletePacket.getUserId()).get()));
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            Main.getInstance().getLogger().error("Netty/Quiz: Error while deleting quiz!", e);
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                        Main.getInstance().getLogger().error("Netty/Quiz: Error while deleting quiz!", (Exception) t);
                    }
                }, MoreExecutors.directExecutor());
                return;
            }

            // Packet for searching quiz
            if (packet instanceof QuizSearchPacket) {
                QuizSearchPacket quizSearchPacket = (QuizSearchPacket) packet;

                Main.getInstance().getUserManager().getUser(quizSearchPacket.getUserId()).ifPresent(user -> {
                    Futures.addCallback(Main.getInstance().getQuizRepository().getQuizByPublicId(quizSearchPacket.getQuizPublicId()), new FutureCallback<Quiz>() {
                        @Override
                        public void onSuccess(Quiz result) {
                            if (result == null) {
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_404, "Dieses Quiz wurde nicht gefunden!"));
                                return;
                            }
                            if(result.getCreatedBy() != user.getId() && result.getCreatedBy() != -1 && !result.isPublicQuiz()) {
                                channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_404, "Dieses Quiz ist nicht öffentlich!"));
                                return;
                            }
                            channel.writeAndFlush(new QuizSearchResponsePacket(result));
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            channel.writeAndFlush(new ErrorPacket(HTTPCodes.ERROR_400, "Es ist ein Fehler aufgetreten!"));
                            Main.getInstance().getLogger().error("Netty/Quiz: Error while fetching quiz!", (Exception) t);
                        }
                    }, MoreExecutors.directExecutor());
                });
            }
        } catch (Exception e) {
            Main.getInstance().getLogger().error("Error while reading packet!", e);
        } finally {
            ReferenceCountUtil.release(object);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // Get address
        String address = ctx.channel().remoteAddress().toString().substring(1);

        this.main.getLogger().info("Netty/User: '" + address + "' connected to the server!");
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // Get address
        String address = ctx.channel().remoteAddress().toString().substring(1);

        // Get User
        Optional<User> optionalUser = Main.getInstance().getUserManager().getUser(ctx.channel());

        // Check if exists
        if (!optionalUser.isPresent()) {
            this.main.getLogger().info("Netty/User: '" + address + "' disconnected from the server!");
            return;
        }

        // Log out
        this.main.getUserManager().logOutUser(optionalUser.get());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        this.main.getLogger().error("Netty-Error", (Exception) cause);
    }
}
