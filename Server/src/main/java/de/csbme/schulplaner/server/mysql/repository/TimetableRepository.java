package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.object.Timetable;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;

import java.time.DayOfWeek;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class TimetableRepository {

    private MySQL mySQL;

    public TimetableRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create timetable table
        try {
            mySQL.execute( "CREATE TABLE IF NOT EXISTS schulplaner_timetable (tt_id INT NOT NULL AUTO_INCREMENT, " +
                    "class_id INT NOT NULL, subject_id INT NOT NULL, day INT NOT NULL, time_hour INT NOT NULL, " +
                    "room VARCHAR(15), teacher VARCHAR(15), FOREIGN KEY (class_id) REFERENCES schulplaner_classes (class_id) ON DELETE CASCADE, " +
                    "FOREIGN KEY (subject_id) REFERENCES schulplaner_subjects (subject_id) ON DELETE CASCADE, " +
                    "PRIMARY KEY (tt_id))" ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "TimetableRepository: Error while creating table 'schulplaner_timetable'", e );
        }
    }

    public ListenableFuture<Integer> insertSchoolHour( int classId, Timetable.SchoolHour schoolHour ) {
        return this.mySQL.execute( "INSERT INTO schulplaner_timetable (class_id, subject_id, day, time_hour, room, teacher) " +
                        "VALUES (?,?,?,?,?,?)", classId, schoolHour.getSubjectId(), schoolHour.getDay().ordinal(),
                schoolHour.getTimeHour().ordinal(), schoolHour.getRoom(), schoolHour.getTeacher() );
    }

    public ListenableFuture<Integer> updateSchoolHour( int classId, Timetable.SchoolHour schoolHour ) {
        return this.mySQL.execute( "UPDATE schulplaner_timetable SET subject_id = ?, room = ?, teacher = ? WHERE tt_id = ?",
                schoolHour.getSubjectId(), schoolHour.getRoom(), schoolHour.getTeacher(), schoolHour.getSchoolHourId() );
    }

    public ListenableFuture<Integer> deleteSchoolHour( int schoolHourId ) {
        return this.mySQL.execute( "DELETE FROM schulplaner_timetable WHERE tt_id = ?", schoolHourId );
    }

    public ListenableFuture<Boolean> existsSchoolHour( int classId, DayOfWeek day, Timetable.TimeHour timeHour ) {
        return Futures.transform( this.mySQL.query( "SELECT * FROM schulplaner_timetable WHERE class_id = ? AND day = ? AND time_hour = ?",
                classId, day.ordinal(), timeHour.ordinal() ), rows -> rows != null && rows.first() != null, MoreExecutors.directExecutor() );
    }

    public ListenableFuture<Timetable> getTimetable( int classId ) {
        return Futures.transform( this.mySQL.query( "SELECT * FROM schulplaner_timetable WHERE class_id = ?", classId ), rows -> {
            if ( rows != null ) {
                return new Timetable( rows.all().stream().map( r -> new Timetable.SchoolHour(
                        r.getInt( "tt_id" ),
                        DayOfWeek.values()[r.getInt( "day" )],
                        Timetable.TimeHour.values()[r.getInt( "time_hour" )],
                        r.getInt( "subject_id" ),
                        r.getString( "room" ),
                        r.getString( "teacher" ) ) ).collect( Collectors.toList() ) );
            }
            return new Timetable();
        }, MoreExecutors.directExecutor() );
    }
}
