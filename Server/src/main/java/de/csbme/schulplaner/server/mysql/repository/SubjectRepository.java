package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.network.packets.subject.SubjectRefreshPacket;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;
import de.csbme.schulplaner.server.user.User;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class SubjectRepository {

    private MySQL mySQL;

    public SubjectRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create subject table
        try {
            mySQL.execute( "CREATE TABLE IF NOT EXISTS schulplaner_subjects (subject_id INT NOT NULL AUTO_INCREMENT, " +
                    "class_id INT NOT NULL, name VARCHAR(50) NOT NULL, short VARCHAR(3) NOT NULL, " +
                    "FOREIGN KEY (class_id) REFERENCES schulplaner_classes (class_id) ON DELETE CASCADE," +
                    "PRIMARY KEY (subject_id))" ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "SubjectRepository: Error while creating table 'schulplaner_subjects'", e );
        }
    }

    public ListenableFuture<List<Subject>> getSubjects( int classId ) {
        return this.getSubjectsByQuery( "SELECT * FROM schulplaner_subjects WHERE class_id = ?", classId );
    }

    public ListenableFuture<Subject> getSubject( int subjectId ) {
        return Futures.transform( this.getSubjectsByQuery( "SELECT * FROM schulplaner_subjects WHERE subject_id = ?", subjectId ), subjects -> {
            if ( subjects != null && subjects.size() > 0 ) {
                return subjects.get( 0 );
            }
            return null;
        }, MoreExecutors.directExecutor() );
    }

    public void sendSubjectsToClassClients( int classId, List<User> users ) {
        Futures.addCallback( Main.getInstance().getSubjectRepository().getSubjects( classId ), new FutureCallback<List<Subject>>() {
            @Override
            public void onSuccess( List<Subject> result ) {
                SubjectRefreshPacket subjectRefreshPacket = new SubjectRefreshPacket( result );
                users.forEach( user -> user.sendPacket( subjectRefreshPacket ) );
            }

            @Override
            public void onFailure( Throwable t ) {
                Main.getInstance().getLogger().error( "User/Subject: Error while fetching subjects!", (Exception) t );
            }
        }, MoreExecutors.directExecutor() );
    }

    public ListenableFuture<Integer> insertSubject( int classId, String name, String shortName ) {
        return this.mySQL.execute( "INSERT INTO schulplaner_subjects (class_id, name, short) " +
                "VALUES (?,?,?)", classId, name, shortName );
    }

    public ListenableFuture<Integer> updateSubject( int subjectId, String name, String shortName ) {
        return this.mySQL.execute( "UPDATE schulplaner_subjects SET name = ?, short = ? WHERE subject_id = ?",
                name, shortName, subjectId );
    }

    public ListenableFuture<Integer> deleteSubject( int subjectId ) {
        return this.mySQL.execute( "DELETE FROM schulplaner_subjects WHERE subject_id = ?", subjectId );
    }

    private ListenableFuture<List<Subject>> getSubjectsByQuery( String sql, Object... objects ) {
        return Futures.transform( this.mySQL.query( sql, objects ), rows -> {
            if ( rows != null ) {
                return rows.all().stream().map( row -> new Subject( row.getInt( "subject_id" ), row.getString( "name" ),
                        row.getString( "short" ) ) ).collect( Collectors.toList() );
            }
            return Collections.emptyList();
        }, MoreExecutors.directExecutor() );
    }
}
