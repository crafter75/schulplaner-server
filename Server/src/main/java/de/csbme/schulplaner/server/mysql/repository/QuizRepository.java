package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.object.Quiz;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;
import de.csbme.schulplaner.server.mysql.row.Rows;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class QuizRepository {

    private MySQL mySQL;

    public QuizRepository(MySQL mySQL) {
        this.mySQL = mySQL;

        // Create quiz table
        try {
            mySQL.execute("CREATE TABLE IF NOT EXISTS schulplaner_quiz (quiz_id INT NOT NULL AUTO_INCREMENT, " +
                    "name VARCHAR(25) NOT NULL, description TEXT, created_by INT NOT NULL, public_quiz TINYINT(1) DEFAULT 0, quiz_public_id INT, date BIGINT NOT NULL, " +
                    "PRIMARY KEY (quiz_id))").get();

            mySQL.execute("CREATE TABLE IF NOT EXISTS schulplaner_quiz_questions (question_id INT NOT NULL AUTO_INCREMENT, " +
                    "quiz_id INT NOT NULL, question VARCHAR(250), answer1 VARCHAR(250), answer2 VARCHAR(250), " +
                    "answer3 VARCHAR(250), answer4 VARCHAR(250), right_answer INT NOT NULL, " +
                    "FOREIGN KEY (quiz_id) REFERENCES schulplaner_quiz (quiz_id) ON DELETE CASCADE, PRIMARY KEY(question_id))").get();
        } catch (InterruptedException | ExecutionException e) {
            Main.getInstance().getLogger().error("QuizRepository: Error while creating table 'schulplaner_quiz'", e);
        }
    }

    public ListenableFuture<List<Quiz>> getAllQuizzes() {
        return this.getQuizzesByQuery("SELECT * FROM schulplaner_quiz");
    }

    public ListenableFuture<List<Quiz>> getOwnAndPreSetsQuizzes(int userId) {
        return this.getQuizzesByQuery("SELECT * FROM schulplaner_quiz WHERE created_by = ? OR created_by = ?", -1, userId);
    }

    public ListenableFuture<Quiz> getQuizByPublicId(int publicQuizId) {
        return Futures.transform(this.getQuizzesByQuery("SELECT * FROM schulplaner_quiz WHERE quiz_public_id = ?", publicQuizId), quizzes -> {
            if (quizzes != null && !quizzes.isEmpty()) {
                return quizzes.get(0);
            }
            return null;
        }, MoreExecutors.directExecutor());
    }

    public ListenableFuture<Integer> insertQuiz(Quiz quiz) {
        return Futures.transformAsync(this.getRandomQuizId(), quizId ->
                this.mySQL.execute("INSERT INTO schulplaner_quiz (name, description, created_by, public_quiz, quiz_public_id, date) " +
                        "VALUES (?,?,?,?,?,?)", quiz.getName(), quiz.getDescription(), quiz.getCreatedBy(), quiz.isPublicQuiz(), quizId, quiz.getDate()), MoreExecutors.directExecutor());
    }

    public ListenableFuture<Integer> updateQuiz(Quiz quiz) {
        return Futures.transform(this.mySQL.execute("UPDATE schulplaner_quiz SET name = ?, description = ?, public_quiz = ? WHERE quiz_id = ?",
                quiz.getName(), quiz.getDescription(), quiz.isPublicQuiz(), quiz.getQuizId()), input -> {
            quiz.getQuestions().forEach(question -> {
                try {
                    this.updateQuestion(quiz.getQuizId(), question).get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
            return input;
        }, MoreExecutors.directExecutor());
    }

    public ListenableFuture<Integer> deleteQuiz(int quizId) {
        return this.mySQL.execute("DELETE FROM schulplaner_quiz WHERE quiz_id = ?", quizId);
    }

    private ListenableFuture<Integer> updateQuestion(int quizId, Quiz.Question question) {
        if (question.getQuestionId() == -1) {
            // Create
            return this.mySQL.execute("INSERT INTO schulplaner_quiz_questions (quiz_id, question, answer1, " +
                            "answer2, answer3, answer4, right_answer) VALUES (?,?,?,?,?,?,?)", quizId,
                    question.getQuestion(), question.getAnswer1(), question.getAnswer2(), question.getAnswer3(),
                    question.getAnswer4(), question.getRightAnswer());
        } else if (question.getQuestion() == null) {
            // Delete
            return this.mySQL.execute("DELETE FROM schulplaner_quiz_questions WHERE question_id = ?", question.getQuestionId());
        } else {
            // Update
            return this.mySQL.execute("UPDATE schulplaner_quiz_questions SET question = ?, answer1 = ?, " +
                            "answer2 = ?, answer3 = ?, answer4 = ?, right_answer = ? WHERE question_id = ?",
                    question.getQuestion(), question.getAnswer1(), question.getAnswer2(), question.getAnswer3(),
                    question.getAnswer4(), question.getRightAnswer(), question.getQuestionId());
        }
    }

    public ListenableFuture<Boolean> existsQuizId(int quizId) {
        return Futures.transform(this.mySQL.query("SELECT quiz_public_id FROM schulplaner_quiz WHERE quiz_public_id = ?", quizId), rows ->
                rows != null && rows.first() != null, MoreExecutors.directExecutor());
    }

    private ListenableFuture<List<Quiz>> getQuizzesByQuery(String sql, Object... objects) {
        return Futures.transform(this.mySQL.query(sql, objects), rows -> {
            List<Quiz> quizzes = new ArrayList<>();

            rows.all().forEach(row -> {
                int quizId = row.getInt("quiz_id");

                try {
                    Rows questionRows = mySQL.query("SELECT * FROM schulplaner_quiz_questions WHERE quiz_id = ?", quizId).get();
                    List<Quiz.Question> questions = questionRows.all().stream().map(r -> new Quiz.Question(
                            r.getInt("question_id"), r.getString("question"),
                            r.getString("answer1"), r.getString("answer2"),
                            r.getString("answer3"), r.getString("answer4"),
                            r.getInt("right_answer")
                    )).collect(Collectors.toList());

                    quizzes.add(new Quiz(row.getInt("quiz_id"), row.getString("name"),
                            row.getString("description"), row.getInt("created_by"),
                            row.getLong("date"), row.getInt("quiz_public_id"),
                            row.getBoolean("public_quiz"), questions));
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
            return quizzes;
        }, MoreExecutors.directExecutor());
    }

    private ListenableFuture<Integer> getRandomQuizId() {
        // Run async
        return this.mySQL.getPool().submit(() -> {
            int quizId;
            Random random = new Random();
            while (true) {
                // Generate id
                quizId = random.nextInt((99999 - 10000)) + 10000;
                // Check if exists
                if (this.existsQuizId(quizId).get()) {
                    continue;
                }
                return quizId;
            }
        });
    }
}
