package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.user.UserGroup;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;
import de.csbme.schulplaner.server.mysql.row.Rows;

import java.util.concurrent.ExecutionException;

public class UserRepository {

    private MySQL mySQL;

    public UserRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create user table
        try {
            mySQL.execute( "CREATE TABLE IF NOT EXISTS schulplaner_users (id INT NOT NULL AUTO_INCREMENT, " +
                    "name VARCHAR(12) DEFAULT NULL, mail_address VARCHAR(50) NOT NULL, password CHAR(128) NOT NULL, " +
                    "registration_date BIGINT NOT NULL, user_group INT NOT NULL DEFAULT " + UserGroup.DEFAULT.ordinal() + ", " +
                    "ip_address VARCHAR(30) DEFAULT NULL, class_id INT DEFAULT -1, PRIMARY KEY (id), UNIQUE(mail_address))" ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "UserRepository: Error while creating table 'schulplaner_users'", e );
        }
    }

    /**
     * Insert a User
     *
     * @param mail     of the user
     * @param password of the user
     * @return the generated User id. the result can be -1 if no id has been generated
     */
    public ListenableFuture<Integer> insertUserAndGetId( String mail, String password ) {
        return this.mySQL.insertAndGetId( "INSERT INTO schulplaner_users (mail_address, password, registration_date) VALUES (?, ?, ?)",
                mail, password, System.currentTimeMillis() );
    }

    /**
     * Get the ID of a User
     *
     * @param mail of the User
     * @return future with the ID of the User
     */
    public ListenableFuture<Integer> getUserIdFromMail( String mail ) {
        // Get user id
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT id FROM schulplaner_users WHERE mail_address = ?", mail );

        // Load user id
        return Futures.transform( futureRows, rows -> {
            // Check if user id found
            if ( rows != null && rows.first() != null ) {
                return rows.first().getInt( "id" );
            }
            return null;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Get the Mail of a User ID
     *
     * @param id of the User
     * @return future with the Mail of the User
     */
    public ListenableFuture<String> getUserMailFromId( int id ) {
        // Get mail
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT mail_address FROM schulplaner_users WHERE id = ?", id );

        // Load user id
        return Futures.transform( futureRows, rows -> {
            // Check if user id found
            if ( rows != null && rows.first() != null ) {
                return rows.first().getString( "mail_address" );
            }
            return null;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Check password of a User
     *
     * @param mail     to check
     * @param password to check
     * @return future if the password is correct
     */
    public ListenableFuture<Boolean> isPasswordCorrect( String mail, String password ) {
        // Get password
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT id FROM schulplaner_users WHERE mail_address = ? AND password = ?", mail, password );

        // Load password
        return Futures.transform( futureRows, rows -> {
            // Check if password correct
            if ( rows != null ) {
                return rows.first() != null;
            }
            return false;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Check if a User exists
     *
     * @param mail to check
     * @return future if the name exists
     */
    public ListenableFuture<Boolean> existsMail( String mail ) {
        return Futures.transform( this.mySQL.query( "SELECT mail_address FROM schulplaner_users WHERE mail_address = ?", mail ), rows -> {
            // Check if exists
            if ( rows != null ) {
                return rows.first() != null;
            }
            return false;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Update address of a User
     *
     * @param userId  of the User
     * @param address to update
     * @return future if the update was successfully
     */
    public ListenableFuture<Integer> updateAddress( int userId, String address ) {
        return this.mySQL.execute( "UPDATE schulplaner_users SET ip_address = ? WHERE id = ?", address, userId );
    }

    /**
     * Update mail of User
     *
     * @apram userId of the USer
     * @param mailAddress to update
     */
    public ListenableFuture<Integer> updateMailAddress(int userId, String mailAddress) {
        return this.mySQL.execute( "UPDATE schulplaner_users SET mail_address = ? WHERE id = ?", mailAddress, userId );
    }

    /**
     * Update password of User
     *
     * @apram userId of the User
     * @param password to update
     */
    public ListenableFuture<Integer> updatePassword(int userId, String password) {
        return this.mySQL.execute( "UPDATE schulplaner_users SET password = ? WHERE id = ?", password, userId );
    }

    /**
     * Update class of User
     *
     * @apram userId of the User
     * @param classId to update
     */
    public ListenableFuture<Integer> updateClass(int userId, int classId) {
        return this.mySQL.execute( "UPDATE schulplaner_users SET class_id = ? WHERE id = ?", classId, userId );
    }

    /**
     * Update name of User
     *
     * @apram userId of the USer
     * @param name to update
     */
    public ListenableFuture<Integer> updateName(int userId, String name) {
        return this.mySQL.execute( "UPDATE schulplaner_users SET name = ? WHERE id = ?", name, userId );
    }

    /**
     * Get Rows of a User
     *
     * @param id of the User
     * @return future with the raw Rows of a User
     */
    public ListenableFuture<Rows> getUserRow( int id ) {
        return this.mySQL.query( "SELECT * FROM schulplaner_users WHERE id = ?", id );
    }

    /**
     * Get Rows of a User
     *
     * @param name of the User
     * @return future with the raw Rows of a User
     */
    public ListenableFuture<Rows> getUserRow( String name ) {
        return this.mySQL.query( "SELECT * FROM schulplaner_users WHERE name = ?", name );
    }

    public ListenableFuture<Integer> updateGroup( int userId, UserGroup group ) {
        return this.mySQL.execute( "UPDATE schulplaner_users SET user_group = ? WHERE id = ?",
                group.ordinal(), userId );
    }
}
