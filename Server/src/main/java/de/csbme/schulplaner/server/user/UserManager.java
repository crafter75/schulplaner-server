package de.csbme.schulplaner.server.user;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.error.HTTPCodes;
import de.csbme.schulplaner.lib.network.packets.ErrorPacket;
import de.csbme.schulplaner.lib.network.packets.user.UserLoginCallbackPacket;
import de.csbme.schulplaner.lib.network.packets.user.UserRegisterCompletePacket;
import de.csbme.schulplaner.lib.object.Class;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.lib.user.UserGroup;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.row.Row;
import de.csbme.schulplaner.server.util.MailSender;
import io.netty.channel.Channel;
import lombok.Getter;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;

public class UserManager {

    private Main main;

    @Getter
    private List<User> users = new CopyOnWriteArrayList<>();

    public UserManager( Main main ) {
        this.main = main;

        // Start time for timeout
        new Timer().scheduleAtFixedRate( new TimerTask() {
            @Override
            public void run() {
                long current = System.currentTimeMillis();

                // Get iterator from list
                Iterator<User> userIterator = UserManager.this.users.iterator();

                // Iterate if has next user
                while ( userIterator.hasNext() ) {
                    // Get user
                    User user = userIterator.next();

                    // Check timeout
                    if ( current > user.getTimeout() ) {
                        // Close channel
                        user.getChannel().close();

                        // Log out
                        UserManager.this.logOutUser( user );
                    }
                }
            }
        }, 3000L, 3000L ); // 3 seconds
    }

    /**
     * Register User
     *
     * @param mailAddress of the User
     * @param password    of the User
     * @param channel     of the User
     */
    public void registerUser( String mailAddress, String password, Channel channel ) {
        Futures.addCallback( this.main.getUserRepository().insertUserAndGetId( mailAddress, password ), new FutureCallback<Integer>() {
            @Override
            public void onSuccess( Integer id ) {
                // Check if Id exists
                if ( id == null ) {
                    UserManager.this.main.getLogger().error( "User: Registered User-ID is null!" );
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_503, "Fehler beim generieren der User-ID!" ) );
                    return;
                }

                // Insert settings
                UserManager.this.main.getSettingRepository().insertUser( id );

                UserManager.this.main.getLogger().info( "Netty/User: '" + mailAddress + "' has been registered!" );

                // Send mail
                Main.POOL.execute( () -> UserManager.this.main.getMailSender().sendMail( mailAddress, MailSender.ContentType.REGISTRATION ) );

                // Send packet to client
                channel.writeAndFlush( new UserRegisterCompletePacket( id, mailAddress ) );
            }

            @Override
            public void onFailure( Throwable throwable ) {
                UserManager.this.main.getLogger().error( "User: Can't register User!", (Exception) throwable );
                channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_503, "Fehler beim registrieren!" ) );
            }
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Login a User
     *
     * @param id      of the User
     * @param channel of the User
     */
    public void loginUser( int id, Channel channel ) {
        try {
            // Get user row
            Row row = this.main.getUserRepository().getUserRow( id ).get().first();

            // Create user
            User user = new User( id, row.getString( "name" ),
                    row.getString( "mail_address" ),
                    row.getLong( "registration_date" ),
                    row.getInt( "class_id" ),
                    UserGroup.values()[row.getInt( "user_group" )],
                    channel );

            // Add to list
            this.users.add( user );

            // Update address
            this.main.getUserRepository().updateAddress( id, user.getAddress() );

            this.main.getLogger().info( "Netty/User: '" + user.getMailAddress() + "' logged in! [" + user.getAddress() + "]" );

            List<Note> notes = this.main.getNoteRepository().getNotes( user.getClassId(), user.getId() ).get();
            List<Subject> subjects = this.main.getSubjectRepository().getSubjects( user.getClassId() ).get();

            Class classInfo = null;
            if ( user.getClassId() != -1 ) {
                classInfo = this.main.getClassRepository().getClassById( user.getClassId() ).get();
            }

            // Send callback packet to client
            user.sendPacket( new UserLoginCallbackPacket( user.getId(), user.getUsername(), user.getMailAddress(),
                    user.getRegistrationDate(), this.main.getSettingRepository().getSettings( id ).get(),
                    user.getGroup(), classInfo, notes, subjects ) );
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "Error while loading user data", e );
            channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_502, "Fehler beim Laden der Daten!" ) );
        }
    }

    /**
     * Log out a User
     *
     * @param user to log out
     */
    public void logOutUser( User user ) {
        // Check if logged in
        if ( !this.users.contains( user ) ) return;

        // Remove user
        this.users.remove( user );
        this.main.getLogger().info( "Netty/User: '" + user.getMailAddress() + "' logged out!" );
    }

    /**
     * Get a User by the mail
     *
     * @param mail of the User
     * @return the User.
     */
    public Optional<User> getUser( String mail ) {
        return this.users.stream().filter( user -> user.getMailAddress().equalsIgnoreCase( mail ) ).findFirst();
    }

    /**
     * Get a User by the id
     *
     * @param id of the User
     * @return the User. the result can be null if the User not found
     */
    public Optional<User> getUser( int id ) {
        return this.users.stream().filter( user -> user.getId() == id ).findFirst();
    }

    /**
     * Get a User by the netty channel
     *
     * @param channel of the User
     * @return the User. the result can be null if the User not found
     */
    public Optional<User> getUser( Channel channel ) {
        return this.users.stream().filter( user -> user.getChannel().equals( channel ) ).findFirst();
    }
}
