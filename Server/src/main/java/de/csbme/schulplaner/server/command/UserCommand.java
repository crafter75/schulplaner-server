package de.csbme.schulplaner.server.command;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.time.TimeUtil;
import de.csbme.schulplaner.lib.user.UserGroup;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.row.Row;
import de.csbme.schulplaner.server.mysql.row.Rows;
import de.csbme.schulplaner.server.util.Logger;
import de.csbme.schulplaner.server.util.command.Command;

public class UserCommand extends Command {

    private Main main;

    public UserCommand( Main main ) {
        super( "user" );
        this.main = main;

        this.getAliases().add( "users" );
        this.getAliases().add( "player" );
        this.getAliases().add( "players" );
    }

    @Override
    public void execute( String label, String[] args ) {
        if ( args.length != 1 ) {
            this.main.getLogger().info( "Usage: /user <ID|Username>" );
            return;
        }

        ListenableFuture<Rows> rows;
        try {
            // Get User by id
            int id = Integer.parseInt( args[0] );

            rows = this.main.getUserRepository().getUserRow( id );
        } catch ( NumberFormatException e ) {
            // Get User by name
            rows = this.main.getUserRepository().getUserRow( args[0] );
        }

        // Load user
        Futures.addCallback( rows, new FutureCallback<Rows>() {
            @Override
            public void onSuccess( Rows rows ) {
                // Check if user exists
                if ( rows == null || rows.first() == null ) {
                    UserCommand.this.main.getLogger().info( "User not found!" );
                    return;
                }
                // Get first row
                Row row = rows.first();
                // Get logger
                Logger logger = UserCommand.this.main.getLogger();
                // Get id from user
                int id = row.getInt( "id" );

                // Log user
                logger.info( "######### User #########" );
                logger.info( "- ID: " + id );
                logger.info( "- Name: " + row.getString( "name" ) );
                logger.info( "- E-Mail: " + row.getString( "mail_address" ) );
                logger.info( "- Registrierungsdatum: " + TimeUtil.getDateFromMilliseconds(
                        row.getLong( "registration_date" ), TimeUtil.DATE_FORMATTER_FULL ) );
                logger.info( "- IP-Adresse: " + row.getString( "ip_address" ) );
                logger.info( "- Gruppe: " + UserGroup.values()[row.getInt( "user_group" )].name());
                logger.info( "######### User #########" );
            }

            @Override
            public void onFailure( Throwable throwable ) {
                UserCommand.this.main.getLogger().error( "Error while fetching User data", (Exception) throwable );
            }
        }, MoreExecutors.directExecutor() );
    }
}
