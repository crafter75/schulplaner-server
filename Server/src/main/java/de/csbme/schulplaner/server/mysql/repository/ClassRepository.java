package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.object.Class;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;
import de.csbme.schulplaner.server.mysql.row.Row;
import de.csbme.schulplaner.server.mysql.row.Rows;
import de.csbme.schulplaner.server.user.User;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class ClassRepository {

    private MySQL mySQL;

    public ClassRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create class table
        try {
            mySQL.execute( "CREATE TABLE IF NOT EXISTS schulplaner_classes (class_id INT NOT NULL AUTO_INCREMENT, " +
                    "name VARCHAR(50) NOT NULL, school_name VARCHAR(250) NOT NULL, created_by INT NOT NULL, " +
                    "class_code INT(5) NOT NULL, FOREIGN KEY (created_by) REFERENCES schulplaner_users (id) ON DELETE CASCADE, " +
                    "PRIMARY KEY (class_id))" ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "ClassRepository: Error while creating table 'schulplaner_classes'", e );
        }
    }

    public ListenableFuture<Integer> insertClass( String name, String schoolName, int createdByUserId ) {
        return Futures.transformAsync( this.getRandomClassCode(), code -> mySQL.insertAndGetId( "INSERT INTO schulplaner_classes (name, school_name, created_by, class_code)" +
                "VALUES (?,?,?,?)", name, schoolName, createdByUserId, code ), this.mySQL.getPool() );
    }

    public ListenableFuture<Boolean> existsClassCode( int classCode ) {
        return Futures.transform( this.mySQL.query( "SELECT class_code FROM schulplaner_classes WHERE class_code = ?", classCode ), (Function<Rows, Boolean>) rows ->
                rows != null && rows.first() != null, MoreExecutors.directExecutor() );
    }

    public ListenableFuture<Class> getClassById( int classId ) {
        return this.getClass( "class_id", classId );
    }

    public ListenableFuture<Class> getClassByCode( int classCode ) {
        return this.getClass( "class_code", classCode );
    }

    public ListenableFuture<Long> getClassMemberSize( int classId ) {
        return Futures.transform( this.mySQL.query( "SELECT COUNT(*) AS size FROM schulplaner_users WHERE class_id = ?", classId ), rows -> {
            if ( rows != null && rows.first() != null ) {
                return rows.first().getLong( "size" );
            }
            return 0L;
        }, MoreExecutors.directExecutor() );
    }

    public ListenableFuture<List<User>> getClassUsers( int classId ) {
        return Futures.transform( this.mySQL.query( "SELECT id FROM schulplaner_users WHERE class_id = ?", classId ), rows -> {
            if ( rows != null ) {
                return rows.all().stream().filter( r -> Main.getInstance().getUserManager().getUser( r.getInt( "id" ) ).isPresent() )
                        .map( r -> Main.getInstance().getUserManager().getUser( r.getInt( "id" ) ).get() ).collect( Collectors.toList() );
            }
            return Collections.emptyList();
        }, MoreExecutors.directExecutor() );
    }

    public ListenableFuture<Boolean> isClassOwner( int classId, int userId ) {
        return Futures.transform( this.mySQL.query( "SELECT * FROM schulplaner_classes WHERE class_id = ? AND created_by = ?",
                classId, userId ), rows -> rows != null && rows.first() != null, MoreExecutors.directExecutor() );
    }

    private ListenableFuture<Class> getClass( String key, int value ) {
        return Futures.transform( this.mySQL.query( "SELECT * FROM schulplaner_classes WHERE " + key + " = ?", value ), rows -> {
            if ( rows != null && rows.first() != null ) {
                Row row = rows.first();

                return new Class( row.getInt( "class_id" ), row.getString( "name" ), row.getString( "school_name" ),
                        row.getInt( "created_by" ), row.getInt( "class_code" ) );
            }
            return null;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Generate a random class code
     *
     * @return unique class code
     */
    private ListenableFuture<Integer> getRandomClassCode() {
        // Run async
        return this.mySQL.getPool().submit( () -> {
            int classCode;
            Random random = new Random();
            while ( true ) {
                // Generate code
                classCode = random.nextInt( ( 99999 - 10000 ) ) + 10000;
                // Check if exists
                if ( this.existsClassCode( classCode ).get() ) {
                    continue;
                }
                return classCode;
            }
        } );
    }
}
