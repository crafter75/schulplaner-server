package de.csbme.schulplaner.server.mysql;

import com.google.common.util.concurrent.FutureCallback;
import de.csbme.schulplaner.server.Main;

@FunctionalInterface
public interface Callback<V> extends FutureCallback<V> {

    @Override
    default void onFailure( Throwable t ) {
        Main.getInstance().getLogger().error( "MySQL: Error while callback execution", (Exception) t );
    }
}
