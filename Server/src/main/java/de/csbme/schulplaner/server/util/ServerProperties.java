package de.csbme.schulplaner.server.util;

import lombok.Getter;

import java.io.*;
import java.util.Properties;

@Getter
public class ServerProperties extends Properties {

    private String mySQLHost, mySQLDatabase, mySQLUser, mySQLPassword, mailAddress, mailPassword;

    private int mySQLPort, nettyPort;

    public ServerProperties( File file ) {
        try {
            // Check if properties file exists
            if ( !file.exists() ) {
                // MySQL
                this.setProperty( "mysql.host", "localhost" );
                this.setProperty( "mysql.database", "database" );
                this.setProperty( "mysql.user", "user" );
                this.setProperty( "mysql.password", "password" );
                this.setProperty( "mysql.port", "3306" );

                // Netty
                this.setProperty( "netty.port", "19130" );

                // Mail
                this.setProperty( "mail.address", "test@gmail.com" );
                this.setProperty( "mail.password", "pw" );

                // Save default properties
                this.store( new FileOutputStream( file ), null );
            }

            // Load properties
            InputStream inputStream = new FileInputStream( file );
            this.load( inputStream );

            // MySQL
            this.mySQLHost = this.getProperty( "mysql.host" );
            this.mySQLDatabase = this.getProperty( "mysql.database" );
            this.mySQLUser = this.getProperty( "mysql.user" );
            this.mySQLPassword = this.getProperty( "mysql.password" );
            this.mySQLPort = Integer.parseInt( this.getProperty( "mysql.port" ) );

            // Netty
            this.nettyPort = Integer.parseInt( this.getProperty( "netty.port" ) );

            // Mail
            this.mailAddress = this.getProperty( "mail.address" );
            this.mailPassword = this.getProperty( "mail.password" );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
