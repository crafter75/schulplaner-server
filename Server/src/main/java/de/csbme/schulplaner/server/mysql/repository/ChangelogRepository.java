package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.object.Changelog;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class ChangelogRepository {

    private MySQL mySQL;

    public ChangelogRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create changelog table
        try {
            mySQL.execute( "CREATE TABLE IF NOT EXISTS schulplaner_changelog (version VARCHAR(15) NOT NULL, " +
                    "date BIGINT NOT NULL, description TEXT, PRIMARY KEY(version))" ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "ChangelogRepository: Error while creating table 'schulplaner_changelog'", e );
        }
    }

    public ListenableFuture<List<Changelog>> getChangelogs() {
        return Futures.transform( this.mySQL.query( "SELECT * FROM schulplaner_changelog" ), rows -> {
            if ( rows != null ) {
                return rows.all().stream().map( row -> new Changelog( row.getString( "version" ),
                        row.getString( "description" ), row.getLong( "date" ) ) ).collect( Collectors.toList() );
            }
            return Collections.emptyList();
        }, MoreExecutors.directExecutor() );
    }
}
