package de.csbme.schulplaner.server.util;

import jline.console.ConsoleReader;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicInteger;

public class Logger {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern( "yyyy-MM-dd" );

    private ConsoleReader reader;

    private org.apache.log4j.Logger fileLogger;

    private org.apache.log4j.Logger apacheLogger;

    public Logger( File logsDirectory ) {
        // Get current date
        LocalDate date = LocalDate.now();

        File file;
        AtomicInteger atomicInteger = new AtomicInteger( 1 );
        while ( true ) {
            // Create file with id and date
            file = new File( logsDirectory, DATE_TIME_FORMATTER.format( date ) + "-" + atomicInteger.getAndIncrement() + ".log" );

            // Check if exists
            if ( file.exists() ) {
                continue;
            }
            break;
        }

        // Check if logs directory exists
        if ( !logsDirectory.exists() ) {
            // No, create new directory
            logsDirectory.mkdir();
        }

        // Set level of root logger to off
        org.apache.log4j.Logger.getRootLogger().setLevel( Level.OFF );

        // Create apache and file logger
        this.apacheLogger = org.apache.log4j.Logger.getLogger( "Kniffel-de.csbme.schulplaner.server.util.Logger" );
        this.fileLogger = org.apache.log4j.Logger.getLogger( "Kniffel-FileLogger" );

        // Create pattern layout and set to logger
        PatternLayout layout = new PatternLayout( "[%d{dd.MM.yyyy HH:mm:ss}] %m%n" );
        ConsoleAppender consoleAppender = new ConsoleAppender( layout );
        this.apacheLogger.addAppender( consoleAppender );

        try {
            // Create file appender and add it to file logger
            FileAppender fileAppender = new FileAppender( layout, file.getAbsolutePath(), false );
            FileAppender fileAppender2 = new FileAppender( layout, logsDirectory.getAbsolutePath() + "/latest.log", false );
            this.fileLogger.addAppender( fileAppender );
            this.fileLogger.addAppender( fileAppender2 );
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        // Set logger level
        this.apacheLogger.setLevel( Level.INFO );
        this.fileLogger.setLevel( Level.INFO );

        try {
            // Create reader
            this.reader = new ConsoleReader( System.in, System.out );
            this.reader.setExpandEvents( false );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    /**
     * logs a normal text into the console
     *
     * @param message the message which should print into the console
     */
    public void info( String message ) {
        this.fileLogger.info( "INFO: " + this.removeColorCodes( message ) );
        this.apacheLogger.info( "INFO: " + message );
    }

    /**
     * logs an error text into the console
     *
     * @param message the message which should print into the console
     */
    public void error( String message ) {
        this.fileLogger.error( "ERROR: " + this.removeColorCodes( message ) );
        this.apacheLogger.error( "ERROR: " + message );
    }

    /**
     * logs an error text and exceptiopn into the console
     *
     * @param message the message which should print into the console
     */
    public void error( String message, Exception e ) {
        this.fileLogger.error( "ERROR: " + this.removeColorCodes( message ), e );
        this.apacheLogger.error( "ERROR: " + message, e );
    }

    /**
     * logs a debug text into the console
     *
     * @param message the message which should print into the console
     */
    public void debug( String message ) {
        this.fileLogger.debug( "DEBUG: " + this.removeColorCodes( message ) );
        this.apacheLogger.debug( "DEBUG: " + message );
    }

    /**
     * logs a warning text into the console
     *
     * @param message the message which should print into the console
     */
    public void warning( String message ) {
        this.fileLogger.warn( "WARN: " + this.removeColorCodes( message ) );
        this.apacheLogger.warn( "WARN: " + message );
    }

    private String removeColorCodes( String message ) {
        String[] list = new String[]{
                "a", "b", "c", "d", "e", "f",
                "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                "r", "l", "n"
        };
        for ( String colorcode : list ) {
            message = message.replaceAll( "§" + colorcode, "" );
        }

        return message;
    }
}
