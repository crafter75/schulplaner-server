package de.csbme.schulplaner.server.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Notification {

    private int notifyId, userId, noteId;

    private boolean playSound, mailNotify;

}
