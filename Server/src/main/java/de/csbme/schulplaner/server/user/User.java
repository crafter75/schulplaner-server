package de.csbme.schulplaner.server.user;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.network.packets.grade.GradeRefreshPacket;
import de.csbme.schulplaner.lib.network.packets.note.NoteRefreshPacket;
import de.csbme.schulplaner.lib.object.Grade;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.user.UserGroup;
import de.csbme.schulplaner.server.Main;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
public class User {

    private final int id;

    @Setter
    private String username;

    @Setter
    private String mailAddress;

    private final long registrationDate;

    @Setter
    private int classId;

    private UserGroup group;

    @Setter
    private Channel channel;

    private long timeout;

    User( int id, String username, String mailAddress, long registrationDate, int classId, UserGroup group, Channel channel ) {
        this.id = id;
        this.username = username;
        this.mailAddress = mailAddress;
        this.registrationDate = registrationDate;
        this.classId = classId;
        this.group = group;
        this.channel = channel;
        this.updateTimeout();
    }

    /**
     * Update timeout of the user
     */
    public void updateTimeout() {
        this.timeout = System.currentTimeMillis() + 30000; // 30 seconds
    }

    /**
     * Get Address of the User
     *
     * @return the Address
     */
    public String getAddress() {
        return this.channel.remoteAddress().toString().substring( 1 ).split( ":" )[0];
    }

    /**
     * Send a packet to the client
     *
     * @param packet to send
     */
    public void sendPacket( Packet packet ) {
        // Check if channel active and open
        if ( !this.channel.isActive() || !this.channel.isOpen() ) {
            Main.getInstance().getLogger().warning( "Netty/User: Sending packet to a closed channel!" );
            return;
        }
        // Send packet
        this.channel.writeAndFlush( packet );
    }

    public void sendNotesToClient() {
        Futures.addCallback( Main.getInstance().getNoteRepository().getNotes( this.classId, this.id ), new FutureCallback<List<Note>>() {
            @Override
            public void onSuccess( List<Note> result ) {
                sendPacket( new NoteRefreshPacket( result ) );
            }

            @Override
            public void onFailure( Throwable t ) {
                Main.getInstance().getLogger().error( "User/Note: Error while fetching notes!", (Exception) t );
            }
        }, MoreExecutors.directExecutor() );
    }

    public void sendGradesToClient() {
        Futures.addCallback( Main.getInstance().getGradeRepository().getGrades( this.id ), new FutureCallback<List<Grade>>() {
            @Override
            public void onSuccess( List<Grade> result ) {
                sendPacket( new GradeRefreshPacket( result ) );
            }

            @Override
            public void onFailure( Throwable t ) {
                Main.getInstance().getLogger().error( "User/Note: Error while fetching grades!", (Exception) t );
            }
        }, MoreExecutors.directExecutor() );
    }
}
