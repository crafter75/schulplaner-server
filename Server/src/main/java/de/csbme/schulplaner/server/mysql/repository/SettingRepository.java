package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.user.Settings;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;
import de.csbme.schulplaner.server.mysql.row.Rows;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;

public class SettingRepository {

    private MySQL mySQL;

    public SettingRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create settings table
        try {
            // Create builder for settings table
            StringBuilder queryBuilder = new StringBuilder( "CREATE TABLE IF NOT EXISTS schulplaner_settings (user_id INT NOT NULL" );

            // Add rows for settings
            Arrays.stream( Settings.Key.values() ).forEach( key -> queryBuilder.append( "," ).append( key.toString() ).append( " " )
                    .append( Settings.getMySQLDataType( key.getClassValue() ) )
                    .append( " NOT NULL DEFAULT " ).append( key.getDefaultValue() ) );

            // Add foreign key and make user_id unique
            queryBuilder.append( ", FOREIGN KEY (user_id) REFERENCES schulplaner_users (id) ON DELETE CASCADE, UNIQUE (user_id))" );

            // Execute query
            mySQL.execute( queryBuilder.toString() ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "SettingRepository: Error while creating table 'schulplaner_settings'", e );
        }
    }

    /**
     * Insert a user
     *
     * @param userId to insert
     * @return future if the user successfully inserted
     */
    public ListenableFuture<Integer> insertUser( int userId ) {
        return this.mySQL.execute( "INSERT INTO schulplaner_settings (user_id) VALUES (?)", userId );
    }

    /**
     * Get the Settings of a User
     *
     * @param userId of the settings
     * @return future with the settings. if no settings found it will return a empty Settings object
     */
    public ListenableFuture<Settings> getSettings( int userId ) {
        // Get settings from user id
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT * FROM schulplaner_settings WHERE user_id = ?", userId );

        // Load settings
        return Futures.transform( futureRows, rows -> {
            // Create settings object
            Settings settings = new Settings();

            // Check if settings exists
            if ( rows != null && rows.first() != null ) {
                // Add settings to object
                rows.first().getValues().entrySet().stream().filter( e -> !e.getKey().equals( "user_id" ) )
                        .forEach( e -> settings.manipulate( Settings.Key.valueOf( e.getKey().toUpperCase() ), e.getValue() ) );
            }
            return settings;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Manipulate setting of a User
     *
     * @param userId  of the User
     * @param setting to update
     * @param value   to update
     */
    public ListenableFuture<Integer> manipulateSetting( int userId, Settings.Key setting, Object value ) {
        // Check if value has the same class
        if ( !setting.getClassValue().getSimpleName().equals( value.getClass().getSimpleName() ) ) {
            throw new IllegalArgumentException( "Can't cast " + value.getClass().getSimpleName() + " to " + setting.getClassValue().getSimpleName() );
        }
        return this.mySQL.execute( "UPDATE schulplaner_settings SET " + setting.toString() + " = ? WHERE user_id = ?",
                value, userId );
    }
}