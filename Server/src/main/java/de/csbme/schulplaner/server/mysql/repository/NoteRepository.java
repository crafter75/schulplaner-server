package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class NoteRepository {

    private MySQL mySQL;

    public NoteRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create notes table
        try {
            mySQL.execute( "CREATE TABLE IF NOT EXISTS schulplaner_notes (note_id INT NOT NULL AUTO_INCREMENT, " +
                    "owner_type INT NOT NULL, owner_id INT NOT NULL, subject_id INT DEFAULT -1, title VARCHAR(25), content TEXT," +
                    "create_date BIGINT NOT NULL, update_date BIGINT NOT NULL, category INT NOT NULL, done TINYINT(1) NOT NULL DEFAULT 0, " +
                    "PRIMARY KEY (note_id))" ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "NoteRepository: Error while creating table 'schulplaner_notes'", e );
        }
    }

    public ListenableFuture<List<Note>> getNotes( int classId, int userId ) {
        if ( classId != -1 ) {
            return this.getNotesByQuery( "SELECT notes.*, IFNULL(notify.date, -1) AS expire_date FROM schulplaner_notes AS notes "
                            + "LEFT JOIN schulplaner_notifications AS notify USING(note_id) WHERE (owner_type = ? AND owner_id = ?) "
                            + "OR (owner_type = ? AND owner_id = ?)", Note.OwnerType.CLASS.ordinal(), classId,
                    Note.OwnerType.USER.ordinal(), userId );
        }
        return this.getNotesByQuery( "SELECT notes.*, IFNULL(notify.date, -1) AS expire_date FROM schulplaner_notes AS notes "
                        + "LEFT JOIN schulplaner_notifications AS notify USING(note_id) WHERE owner_type = ? AND owner_id = ?",
                Note.OwnerType.USER.ordinal(), userId );
    }

    public ListenableFuture<Note> getNote( int noteId ) {
        return Futures.transform( this.getNotesByQuery( "SELECT notes.*, IFNULL(notify.date, -1) AS expire_date FROM schulplaner_notes AS notes "
                + "LEFT JOIN schulplaner_notifications AS notify USING(note_id) WHERE note_id = ?", noteId ), notes -> {
                    if ( notes != null && notes.size() > 0 ) {
                        return notes.get( 0 );
                    }
                    return null;
                }, MoreExecutors.directExecutor() );
    }

    public ListenableFuture<Integer> insertNote( Note.OwnerType ownerType, int ownerId, Note note ) {
        return this.mySQL.insertAndGetId( "INSERT INTO schulplaner_notes (owner_type, owner_id, subject_id, title, content, "
                        + "create_date, update_date, category) VALUES (?,?,?,?,?,?,?,?)", ownerType.ordinal(), ownerId, note.getSubjectId(),
                note.getTitle(), note.getContent(), System.currentTimeMillis(), System.currentTimeMillis(), note.getCategory() );
    }

    public ListenableFuture<Integer> updateNote( Note note ) {
        return this.mySQL.execute( "UPDATE schulplaner_notes SET owner_type = ?, subject_id = ?, title = ?, content = ?, "
                        + "category = ?, done = ?, update_date = ? WHERE note_id = ?", note.getOwnerType(), note.getSubjectId(),
                note.getTitle(), note.getContent(), note.getCategory(), note.isDone(), System.currentTimeMillis(), note.getNoteId() );
    }

    public ListenableFuture<Integer> deleteNote( int noteId ) {
        return this.mySQL.execute( "DELETE FROM schulplaner_notes WHERE note_id = ?", noteId );
    }

    private ListenableFuture<List<Note>> getNotesByQuery( String sql, Object... objects ) {
        return Futures.transform( this.mySQL.query( sql, objects ), rows -> {
            if ( rows != null ) {
                return rows.all().stream().map( row -> new Note( row.getInt( "note_id" ),
                        row.getInt( "subject_id" ), row.getInt( "category" ),
                        row.getInt( "owner_type" ), row.getString( "title" ),
                        row.getString( "content" ), row.getLong( "create_date" ),
                        row.getLong( "update_date" ), row.getLong( "expire_date" ),
                        row.getBoolean( "done" ) ) ).collect( Collectors.toList() );
            }
            return Collections.emptyList();
        }, MoreExecutors.directExecutor() );
    }
}
