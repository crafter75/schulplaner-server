package de.csbme.schulplaner.server.util.command;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class Command {

    private String name;

    private List<String> aliases = new ArrayList<>();

    public Command( String name ) {
        this.name = name;
    }

    public Command( String name, List<String> aliases ) {
        this.name = name;
        this.aliases = aliases;
    }

    /**
     * Execute a command
     *
     * @param label of the command
     * @param args  for the command
     */
    public abstract void execute( String label, String[] args );
}
