package de.csbme.schulplaner.server;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.network.packets.note.NoteReminderPacket;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.lib.time.TimeUtil;
import de.csbme.schulplaner.lib.user.Category;
import de.csbme.schulplaner.server.command.HelpCommand;
import de.csbme.schulplaner.server.command.StopCommand;
import de.csbme.schulplaner.server.command.UserCommand;
import de.csbme.schulplaner.server.mysql.MySQL;
import de.csbme.schulplaner.server.mysql.repository.*;
import de.csbme.schulplaner.server.network.NettyServer;
import de.csbme.schulplaner.server.user.User;
import de.csbme.schulplaner.server.user.UserManager;
import de.csbme.schulplaner.server.util.Logger;
import de.csbme.schulplaner.server.util.MailSender;
import de.csbme.schulplaner.server.util.Notification;
import de.csbme.schulplaner.server.util.ServerProperties;
import de.csbme.schulplaner.server.util.command.CommandHandler;
import lombok.Getter;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Getter
public class Main {

    public static final ExecutorService POOL = Executors.newCachedThreadPool();

    @Getter
    private static Main instance;

    private Logger logger;

    private ServerProperties serverProperties;

    private MySQL mySQL;

    private CommandHandler commandHandler;

    private NettyServer nettyServer;

    private ChangelogRepository changelogRepository;

    private ClassRepository classRepository;

    private GradeRepository gradeRepository;

    private NoteRepository noteRepository;

    private NotificationRepository notificationRepository;

    private QuizRepository quizRepository;

    private SettingRepository settingRepository;

    private SubjectRepository subjectRepository;

    private TimetableRepository timetableRepository;

    private UserRepository userRepository;

    private UserManager userManager;

    private MailSender mailSender;

    private Main() {
        // Save instance
        instance = this;

        // Init logger
        this.logger = new Logger( new File( "logs" ) );
        this.logger.info( "Starting server..." );

        // Add shutdown hook
        Runtime.getRuntime().addShutdownHook( new Thread( () -> {
            this.logger.info( "Stopping..." );

            // Disconnect from mysql
            this.mySQL.disconnect();

            // Shutdown pool
            POOL.shutdown();

            this.logger.info( "Server successfully stopped!" );
        } ) );

        // Init and load properties
        this.serverProperties = new ServerProperties( new File( "server.properties" ) );

        // Init mail sender
        this.mailSender = new MailSender( this.serverProperties );

        // Init mysql connection
        this.mySQL = new MySQL( this.serverProperties );

        // Check connection
        if ( !this.mySQL.isConnected() ) {
            return;
        }

        // Init command handler
        this.commandHandler = new CommandHandler();
        POOL.execute( () -> this.commandHandler.consoleInput( command ->
                this.logger.info( "[CommandHandler] The command '" + command + "' not exists." ) ) );

        // Register command
        this.commandHandler.registerCommand( new HelpCommand( this ) );
        this.commandHandler.registerCommand( new StopCommand( this ) );
        this.commandHandler.registerCommand( new UserCommand( this ) );

        // Init netty server
        this.nettyServer = new NettyServer( this, this.serverProperties.getNettyPort() );
        this.nettyServer.startServer();

        // Init repositories
        this.userRepository = new UserRepository( this.mySQL );
        this.changelogRepository = new ChangelogRepository( this.mySQL );
        this.noteRepository = new NoteRepository( this.mySQL );
        this.notificationRepository = new NotificationRepository( this.mySQL );
        this.quizRepository = new QuizRepository( this.mySQL );
        this.settingRepository = new SettingRepository( this.mySQL );
        this.classRepository = new ClassRepository( this.mySQL );
        this.gradeRepository = new GradeRepository( this.mySQL );
        this.subjectRepository = new SubjectRepository( this.mySQL );
        this.timetableRepository = new TimetableRepository( this.mySQL );

        // Init manager
        this.userManager = new UserManager( this );

        // Start reminder task
        POOL.execute( () -> {
            new Timer().scheduleAtFixedRate( new TimerTask() {
                @Override
                public void run() {
                    Futures.addCallback( Main.this.notificationRepository.getExpiredNotifications(), new FutureCallback<List<Notification>>() {
                        @Override
                        public void onSuccess( List<Notification> notifications ) {
                            List<Integer> notifyIdsToDelete = notifications.stream().map( Notification::getNotifyId ).collect( Collectors.toList() );
                            if ( notifyIdsToDelete.isEmpty() ) {
                                return;
                            }

                            // Delete ids
                            Futures.addCallback( Main.this.notificationRepository.deleteNotifications( notifyIdsToDelete ), new FutureCallback<Integer>() {
                                @Override
                                public void onSuccess( Integer result ) {
                                    try {
                                        Set<User> refreshUsers = new HashSet<>();
                                        for ( Notification n : notifications ) {
                                            Note note = Main.this.noteRepository.getNote( n.getNoteId() ).get();
                                            if ( note == null ) {
                                                Main.this.getLogger().warning( "Scheduler/ExpiredNotifications: Note with ID " + n.getNoteId() + " is null!" );
                                                continue;
                                            }

                                            Optional<User> optional = Main.this.userManager.getUser( n.getUserId() );
                                            if ( n.isMailNotify() ) {
                                                String mail = optional.map( User::getMailAddress ).orElse( Main.this.userRepository.getUserMailFromId( n.getUserId() ).get() );
                                                MailSender.ContentType type;
                                                Object[] titleObjects, contentObjects;
                                                if ( note.getTitle() == null || note.getTitle().isEmpty() ) {
                                                    Subject subject = Main.this.subjectRepository.getSubject( note.getSubjectId() ).get();
                                                    if ( subject == null ) {
                                                        Main.this.getLogger().warning( "Scheduler/ExpiredNotifications: Subject with ID " + note.getSubjectId() + " is null!" );
                                                        continue;
                                                    }
                                                    type = MailSender.ContentType.NOTIFICATION_ENTRY;
                                                    titleObjects = new Object[]{ subject.getName() + " " + Category.values()[note.getCategory()].getDisplayName() };
                                                    contentObjects = new Object[]{
                                                            subject.getName(),
                                                            Category.values()[note.getCategory()].getDisplayName(),
                                                            TimeUtil.getDateFromMilliseconds( note.getCreateDate(), TimeUtil.DATE_WITHOUT_SECONDS ),
                                                            TimeUtil.getDateFromMilliseconds( note.getUpdateDate(), TimeUtil.DATE_WITHOUT_SECONDS ),
                                                            note.getContent()
                                                    };
                                                } else {
                                                    type = MailSender.ContentType.NOTIFICATION_NOTE;
                                                    titleObjects = new Object[]{ note.getTitle() };
                                                    contentObjects = new Object[]{
                                                            note.getTitle(),
                                                            TimeUtil.getDateFromMilliseconds( note.getCreateDate(), TimeUtil.DATE_WITHOUT_SECONDS ),
                                                            TimeUtil.getDateFromMilliseconds( note.getUpdateDate(), TimeUtil.DATE_WITHOUT_SECONDS ),
                                                            note.getContent()
                                                    };
                                                }
                                                POOL.execute( () -> Main.this.mailSender.sendMail( mail, type, titleObjects, contentObjects ) );
                                            }
                                            if ( !optional.isPresent() ) {
                                                continue;
                                            }
                                            refreshUsers.add( optional.get() );
                                            optional.get().sendPacket( new NoteReminderPacket( optional.get().getId(), note, n.isPlaySound() ) );
                                        }
                                        refreshUsers.forEach( User::sendNotesToClient );
                                    } catch ( InterruptedException | ExecutionException e ) {
                                        Main.this.getLogger().error( "Scheduler/ExpiredNotifications: Error while sending expired notifications!", e );
                                    }
                                }

                                @Override
                                public void onFailure( Throwable t ) {
                                    Main.this.getLogger().error( "Scheduler/ExpiredNotifications: Error while deleting expired notifications!", (Exception) t );
                                }
                            }, MoreExecutors.directExecutor() );
                        }

                        @Override
                        public void onFailure( Throwable t ) {
                            Main.this.getLogger().error( "Scheduler/ExpiredNotifications: Error while fetching expired notifications!", (Exception) t );
                        }
                    }, MoreExecutors.directExecutor() );
                }
            }, TimeUnit.SECONDS.toMillis( 1L ), TimeUnit.SECONDS.toMillis( 30L ) ); // 1 sec delay, 30 sec period
        } );
    }

    public static void main( String[] args ) {
        new Main();
    }

    private void addLine( StringBuilder builder, String line ) {
        builder.append( line ).append( "\n" );
    }
}
