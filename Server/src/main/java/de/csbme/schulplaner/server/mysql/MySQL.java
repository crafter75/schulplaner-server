package de.csbme.schulplaner.server.mysql;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.row.Row;
import de.csbme.schulplaner.server.mysql.row.Rows;
import de.csbme.schulplaner.server.util.ServerProperties;
import lombok.Getter;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

public class MySQL {

    private Connection connection;

    @Getter
    private ListeningExecutorService pool = MoreExecutors.listeningDecorator( Executors.newCachedThreadPool() );

    public MySQL( ServerProperties serverProperties ) {
        Main.getInstance().getLogger().info( "MySQL: Connecting..." );
        if ( this.connect( serverProperties ) ) {
            Main.getInstance().getLogger().info( "MySQL: Connected!" );
            return;
        }
        Main.getInstance().getLogger().error( "MySQL: Can't connect to database!" );
        System.exit( 0 );
    }

    /**
     * Connect to the mysql server
     *
     * @param serverProperties with the connection data
     * @return if the connection is ready
     */
    private boolean connect( ServerProperties serverProperties ) {
        try {
            // Check if class exists
            Class.forName( "com.mysql.jdbc.Driver" );

            // Setup connection
            this.connection = DriverManager.getConnection( "jdbc:mysql://" + serverProperties.getMySQLHost() + ":"
                    + serverProperties.getMySQLPort() + "/" + serverProperties.getMySQLDatabase() + "?user="
                    + serverProperties.getMySQLUser() + "&password=" + serverProperties.getMySQLPassword()
                    + "&autoReconnect=true" );
        } catch ( Exception e ) {
            Main.getInstance().getLogger().error( "MySQL: Connection error!", e );
        }
        return this.isConnected();
    }

    /**
     * Disconnect from the mysql server
     *
     * @return if the connection successfully closed
     */
    public boolean disconnect() {
        if ( !this.isConnected() ) {
            return true;
        }

        try {
            this.connection.close();
            return true;
        } catch ( SQLException e ) {
            Main.getInstance().getLogger().info( "MySQL: Can't close connection!" );
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Check the connection
     *
     * @return if the mysql connection is alive
     */
    public boolean isConnected() {
        try {
            return this.connection != null && !this.connection.isClosed();
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Insert a async statement
     *
     * @param statement to execute
     * @param objects   as parameter
     * @return future with the generated ID. the result can be null
     */
    public ListenableFuture<Integer> insertAndGetId( String statement, Object... objects ) {
        // Check if connected
        if ( !this.isConnected() ) {
            return Futures.immediateFailedFuture( new SQLException( "No MySQL connection!" ) );
        }

        // Execute in other thread
        return this.pool.submit( () -> {
            try {
                // Get Prepared Statement
                PreparedStatement preparedStatement = this.getPreparedStatement( statement, objects );

                // Execute statement
                preparedStatement.execute();

                // Get ResultSet
                ResultSet resultSet = preparedStatement.getGeneratedKeys();

                Integer result = null;

                // Check if a ID has been generated
                if ( resultSet.next() ) {
                    result = resultSet.getInt( 1 );
                }

                // Close
                resultSet.close();
                preparedStatement.close();

                return result;
            } catch ( SQLException e ) {
                Main.getInstance().getLogger().error( "[MySQL] Error while inserting statement!", e );
                return null;
            }
        } );
    }

    /**
     * Execute a async statement
     *
     * @param statement to execute
     * @param objects   as parameter
     * @return future with the changed rows
     */
    public ListenableFuture<Integer> execute( String statement, Object... objects ) {
        // Check if connected
        if ( !this.isConnected() ) {
            return Futures.immediateFailedFuture( new SQLException( "No MySQL connection!" ) );
        }

        // Execute in other thread
        return this.pool.submit( () -> {
            try {
                // Get Prepared Statement
                PreparedStatement preparedStatement = this.getPreparedStatement( statement, objects );

                // Execute statement
                Integer result = preparedStatement.executeUpdate();

                // Close statement
                preparedStatement.close();

                return result;
            } catch ( SQLException e ) {
                Main.getInstance().getLogger().error( "[MySQL] Error while executing statement!", e );
                return 0;
            }
        } );
    }

    /**
     * Query a async statement
     *
     * @param statement to execute
     * @param objects   as parameter
     * @return future with the Rows as result
     */
    public ListenableFuture<Rows> query( String statement, Object... objects ) {
        // Check if connected
        if ( !this.isConnected() ) {
            return Futures.immediateFailedFuture( new SQLException( "No MySQL connection!" ) );
        }

        // Execute in other thread
        return this.pool.submit( () -> {
            try {
                // Get Prepared Statement
                PreparedStatement preparedStatement = this.getPreparedStatement( statement, objects );

                // Execute statement and get ResultSet
                ResultSet resultSet = preparedStatement.executeQuery();

                // Cache ResultSet into Rows
                Rows rows = this.getRows( resultSet );

                // Close statement
                preparedStatement.close();
                resultSet.close();

                return rows;
            } catch ( SQLException e ) {
                Main.getInstance().getLogger().error( "[MySQL] Error while query statement!", e );
                return new Rows();
            }
        } );
    }

    public boolean existsTable( String table ) {
        try {
            ResultSet rs = this.connection.getMetaData().getTables( null, null, table, null );
            boolean result = rs.next();
            rs.close();
            return result;
        } catch ( SQLException e ) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get the prepared statement
     *
     * @param statement to prepare
     * @param objects   to set
     * @return the prepared statement
     */
    private PreparedStatement getPreparedStatement( String statement, Object... objects ) throws SQLException {
        // Create PreparedStatement with the connection
        PreparedStatement preparedStatement = this.connection.prepareStatement( statement, Statement.RETURN_GENERATED_KEYS );

        // Set variable objects to parse. This prevents also sql injections :)
        for ( int i = 0; i < objects.length; i++ ) {
            preparedStatement.setObject( ( i + 1 ), objects[i] );
        }
        return preparedStatement;
    }

    /**
     * Get Rows from ResultSet
     *
     * @param resultSet to parse
     * @return Rows
     */
    private Rows getRows( ResultSet resultSet ) throws SQLException {
        // Create empty list
        List<Row> rowList = new ArrayList<>();

        // Get metadata from result set
        ResultSetMetaData metaData = resultSet.getMetaData();

        // Get column count from meta data
        int columnCount = metaData.getColumnCount();

        // Create map
        Map<String, Object> row;

        // Iterate result set
        while ( resultSet.next() ) {
            // Init map with size of column count
            row = new HashMap<>( columnCount );

            // Iterate column count
            for ( int i = 1; i <= columnCount; i++ ) {
                // Put column name with result
                if ( metaData.getColumnType( i ) == Types.DECIMAL ) {
                    row.put( metaData.getColumnName( i ).toLowerCase(), resultSet.getBigDecimal( i ).doubleValue() );
                    continue;
                }
                row.put( metaData.getColumnName( i ).toLowerCase(), resultSet.getObject( i ) );
            }
            // Add to list
            rowList.add( new Row( row ) );
        }
        return new Rows( rowList );
    }
}
