package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;
import de.csbme.schulplaner.server.mysql.row.Rows;
import de.csbme.schulplaner.server.util.Notification;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class NotificationRepository {

    private MySQL mySQL;

    public NotificationRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create notification table
        try {
            mySQL.execute( "CREATE TABLE IF NOT EXISTS schulplaner_notifications (notify_id INT NOT NULL AUTO_INCREMENT, " +
                    "user_id INT NOT NULL, note_id INT NOT NULL, date BIGINT NOT NULL, play_sound TINYINT(1) NOT NULL DEFAULT 1, " +
                    "mail_notify TINYINT(1) NOT NULL DEFAULT 1, FOREIGN KEY (user_id) REFERENCES schulplaner_users (id) ON DELETE CASCADE," +
                    "FOREIGN KEY (note_id) REFERENCES schulplaner_notes (note_id) ON DELETE CASCADE, PRIMARY KEY (notify_id))" ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "NotificationRepository: Error while creating table 'schulplaner_notifications'", e );
        }
    }

    public ListenableFuture<List<Notification>> getExpiredNotifications() {
        return Futures.transform( this.mySQL.query( "SELECT * FROM schulplaner_notifications WHERE date < ?", System.currentTimeMillis() ), rows -> {
            if ( rows != null ) {
                return rows.all().stream().map( row -> new Notification( row.getInt( "notify_id" ),
                        row.getInt( "user_id" ), row.getInt( "note_id" ),
                        row.getBoolean( "play_sound" ), row.getBoolean( "mail_notify" ) ) ).collect( Collectors.toList() );
            }
            return Collections.emptyList();
        }, MoreExecutors.directExecutor() );
    }

    public ListenableFuture<Integer> updateNotification( int userId, int noteId, long date ) {
        if ( date == -1L ) {
            // Delete notification if exists
            return this.deleteNotification( userId, noteId );
        }
        return Futures.transformAsync( this.existsNotification( userId, noteId ), exists -> {
            if ( exists ) {
                return this.mySQL.execute( "UPDATE schulplaner_notifications SET date = ? WHERE user_id = ? AND note_id = ?",
                        date, userId, noteId );
            }
            return this.mySQL.execute( "INSERT INTO schulplaner_notifications (user_id, note_id, date) VALUES (?,?,?) ",
                    userId, noteId, date );
        }, MoreExecutors.directExecutor() );
    }

    public ListenableFuture<Integer> deleteNotifications( List<Integer> list ) {
        if(list.isEmpty()) {
            return Futures.immediateFuture( 0 );
        }
        StringBuilder stringBuilder = new StringBuilder();
        list.forEach( i -> {
            if ( stringBuilder.length() == 0 ) {
                stringBuilder.append( i );
                return;
            }
            stringBuilder.append( "," ).append( i );
        } );
        return this.mySQL.execute( "DELETE FROM schulplaner_notifications WHERE notify_id IN (" + stringBuilder.toString() + ")" );
    }

    public ListenableFuture<Integer> deleteNotification( int userId, int noteId ) {
        return this.mySQL.execute( "DELETE FROM schulplaner_notifications WHERE user_id = ? AND note_id = ?", userId, noteId );
    }

    private ListenableFuture<Boolean> existsNotification( int userId, int noteId ) {
        return Futures.transform( this.mySQL.query( "SELECT * FROM schulplaner_notifications WHERE user_id = ? AND note_id = ?",
                userId, noteId ), rows -> rows != null && rows.first() != null, MoreExecutors.directExecutor() );
    }
}
