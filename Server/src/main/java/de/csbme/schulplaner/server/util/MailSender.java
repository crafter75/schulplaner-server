package de.csbme.schulplaner.server.util;

import de.csbme.schulplaner.server.Main;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.util.Arrays;
import java.util.Properties;

public class MailSender {

    private ServerProperties serverProperties;

    private Session session;

    public MailSender( ServerProperties serverProperties ) {
        this.serverProperties = serverProperties;
        this.session = Session.getInstance( this.getHostProperties(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication( serverProperties.getMailAddress(), serverProperties.getMailPassword() );
            }
        } );
    }

    public void sendMail( String to, ContentType contentType, Object[] titleObjects, Object[] contentObjects ) {
        this.sendMassMail( new String[]{ to }, contentType, titleObjects, contentObjects );
    }

    public void sendMail( String to, ContentType contentType, Object... contentObjects ) {
        this.sendMassMail( new String[]{ to }, contentType, new Object[0], contentObjects );
    }

    public void sendMassMail( String[] to, ContentType contentType, Object[] titleObjects, Object[] contentObjects ) {
        try {
            if ( this.session == null ) {
                throw new NullPointerException( "Mail: Session is null!" );
            }
            StringBuilder builder = new StringBuilder();
            Arrays.stream( to ).forEach( mail -> {
                if ( builder.length() == 0 ) {
                    builder.append( mail );
                    return;
                }
                builder.append( "," ).append( mail );
            } );
            String title = contentType.getTitle();
            String content = contentType.getContent();

            if ( content == null ) {
                return;
            }

            for ( int i = 0; i < titleObjects.length; i++ ) {
                title = title.replace( "{" + i + "}", "" + titleObjects[i] );
            }

            for ( int i = 0; i < contentObjects.length; i++ ) {
                content = content.replace( "{" + i + "}", "" + contentObjects[i] );
            }
            Message message = new MimeMessage( this.session );
            message.setFrom( new InternetAddress( this.serverProperties.getMailAddress() ) );
            message.setRecipients( Message.RecipientType.TO, InternetAddress.parse( builder.toString() ) );
            message.setSubject( title );
            message.setContent( content, "text/html" );
            Transport.send( message );
        } catch ( Exception e ) {
            Main.getInstance().getLogger().error( "Mail: Error while sending mail!", e );
            e.printStackTrace();
        }
    }

    private Properties getHostProperties() {
        Properties properties = new Properties();
        properties.put( "mail.smtp.host", "smtp.gmail.com" );
        properties.put( "mail.smtp.port", "587" );
        properties.put( "mail.smtp.auth", "true" );
        properties.put( "mail.smtp.starttls.enable", "true" );
        return properties;
    }

    @Getter
    @AllArgsConstructor
    public enum ContentType {

        REGISTRATION( "Schulplaner Registrierung", "registration.html" ),
        NOTIFICATION_ENTRY( "Erinnerung an {0}", "notification_entry.html" ),
        NOTIFICATION_NOTE( "Erinnerung an '{0}'", "notification_note.html" );

        private String title, fileName;

        public String getContent() {
            try {
                StringBuilder sb = new StringBuilder();
                InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream( this.fileName );
                if ( inputStream == null ) {
                    throw new NullPointerException( "File '" + this.fileName + "' is null!" );
                }
                BufferedReader reader = new BufferedReader( new InputStreamReader( inputStream ) );
                String line;
                while ( ( line = reader.readLine() ) != null ) {
                    sb.append( line );
                }
                inputStream.close();
                reader.close();
                return sb.toString();
            } catch ( Exception e ) {
                Main.getInstance().getLogger().error( "Mail: Error while get content from file", e );
                e.printStackTrace();
                return null;
            }
        }
    }
}
