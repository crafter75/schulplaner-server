package de.csbme.schulplaner.server.mysql.repository;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.schulplaner.lib.object.Grade;
import de.csbme.schulplaner.lib.user.Category;
import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.mysql.MySQL;

import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class GradeRepository {

    private MySQL mySQL;

    public GradeRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create grade table
        try {
            if ( !this.mySQL.existsTable( "schulplaner_grades" ) ) {
                mySQL.execute( "CREATE TABLE IF NOT EXISTS schulplaner_grades (grade_id INT NOT NULL AUTO_INCREMENT, " +
                        "user_id INT NOT NULL, subject_id INT NOT NULL, title VARCHAR(25), grade VARCHAR(50) NOT NULL," +
                        "date BIGINT NOT NULL, category INT NOT NULL, FOREIGN KEY (user_id) REFERENCES schulplaner_users (id) ON DELETE CASCADE," +
                        "PRIMARY KEY (grade_id))" ).get();

                new Timer().schedule( new TimerTask() {
                    @Override
                    public void run() {
                        setForeignKeys();
                    }
                }, 3000L );
            }
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "GradeRepository: Error while creating table 'schulplaner_grades'", e );
        }
    }

    public ListenableFuture<List<Grade>> getGrades( int userId ) {
        return this.getGradesByQuery( "SELECT * FROM schulplaner_grades WHERE user_id = ?", userId );
    }

    public ListenableFuture<List<Grade>> getGrades( int userId, Category category ) {
        return this.getGradesByQuery( "SELECT * FROM schulplaner_grades WHERE user_id = ? AND category = ?", userId, category.ordinal() );
    }

    public ListenableFuture<List<Grade>> getGrades( int userId, int subjectId ) {
        return this.getGradesByQuery( "SELECT * FROM schulplaner_grades WHERE user_id = ? AND subject_id = ?", userId, subjectId );
    }

    public ListenableFuture<List<Grade>> getGrades( int userId, int subjectId, Category category ) {
        return this.getGradesByQuery( "SELECT * FROM schulplaner_grades WHERE user_id = ? AND subject_id = ? AND category = ?", userId, subjectId, category.ordinal() );
    }

    public ListenableFuture<Integer> insertGrade( int userId, Grade grade ) {
        return this.mySQL.execute( "INSERT INTO schulplaner_grades (user_id, subject_id, title, grade, date, category) VALUES (?,?,?,?,?,?)",
                userId, grade.getSubjectId(), grade.getTitle(), grade.getGrade(), String.valueOf( grade.getDate() ), grade.getCategory() );
    }

    public ListenableFuture<Integer> updateGrade( Grade grade ) {
        return this.mySQL.execute( "UPDATE schulplaner_grades SET subject_id = ?, title = ?, grade = ?, date = ?, category = ? WHERE grade_id = ?",
                grade.getSubjectId(), grade.getTitle(), grade.getGrade(), String.valueOf( grade.getDate() ), grade.getCategory(), grade.getGradeId() );
    }

    public ListenableFuture<Integer> deleteGrade( int gradeId ) {
        return this.mySQL.execute( "DELETE FROM schulplaner_grades WHERE grade_id = ?", gradeId );
    }

    private ListenableFuture<List<Grade>> getGradesByQuery( String sql, Object... objects ) {
        return Futures.transform( this.mySQL.query( sql, objects ), rows -> {
            if ( rows != null ) {
                return rows.all().stream().map( row -> new Grade( row.getInt( "grade_id" ), Double.parseDouble( row.getString( "grade" ) ),
                        row.getInt( "subject_id" ), row.getInt( "category" ), row.getLong( "date" ), row.getString( "title" ) ) ).collect( Collectors.toList() );
            }
            return Collections.emptyList();
        }, MoreExecutors.directExecutor() );
    }

    private void setForeignKeys() {
        try {
            this.mySQL.execute( "ALTER TABLE schulplaner_grades ADD CONSTRAINT fk_subject_id FOREIGN KEY (subject_id) REFERENCES schulplaner_subjects(subject_id)" ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "GradeRepository: Error while adding foreign key to table 'schulplaner_grades'", e );
        }
    }
}
