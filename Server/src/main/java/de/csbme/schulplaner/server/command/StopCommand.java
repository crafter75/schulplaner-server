package de.csbme.schulplaner.server.command;

import de.csbme.schulplaner.server.Main;
import de.csbme.schulplaner.server.util.command.Command;

public class StopCommand extends Command {

    private Main main;

    public StopCommand( Main main ) {
        super( "stop" );
        this.getAliases().add( "shutdown" );
        this.getAliases().add( "end" );
        this.main = main;
    }

    @Override
    public void execute( String label, String[] args ) {
        this.main.getLogger().info( "Stopping server..." );
        System.exit( 0 );
    }
}
