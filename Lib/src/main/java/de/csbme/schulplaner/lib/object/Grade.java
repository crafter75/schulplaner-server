package de.csbme.schulplaner.lib.object;

import com.google.gson.Gson;
import de.csbme.schulplaner.lib.user.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class Grade {

    private static final Gson gson = new Gson();

    private int gradeId;

    @Setter
    private double grade;

    @Setter
    private int subjectId, category;

    @Setter
    private long date;

    @Setter
    private String title;

    public Grade( double grade, int subjectId, Category category, String title, long date ) {
        this.gradeId = -1;
        this.grade = grade;
        this.subjectId = subjectId;
        this.category = category.ordinal();
        this.date = date;
        this.title = title;
    }

    public static Grade fromString( String data ) {
        return gson.fromJson( data, Grade.class );
    }

    public static String toString( Grade grade ) {
        return gson.toJson( grade );
    }
}
