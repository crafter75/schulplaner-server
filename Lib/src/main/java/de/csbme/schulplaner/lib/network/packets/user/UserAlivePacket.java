package de.csbme.schulplaner.lib.network.packets.user;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserAlivePacket extends Packet {

    private int id;
}
