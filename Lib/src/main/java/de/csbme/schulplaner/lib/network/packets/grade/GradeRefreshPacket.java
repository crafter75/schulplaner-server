package de.csbme.schulplaner.lib.network.packets.grade;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Grade;

import java.util.List;
import java.util.stream.Collectors;

public class GradeRefreshPacket extends Packet {

    private List<String> grades;

    public GradeRefreshPacket( List<Grade> grades ) {
        this.grades = grades.stream().map( g -> Grade.toString( g ) ).collect( Collectors.toList() );
    }

    public List<Grade> getGrades() {
        return this.grades.stream().map( Grade::fromString ).collect( Collectors.toList() );
    }
}
