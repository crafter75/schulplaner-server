package de.csbme.schulplaner.lib.object;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Subject {

    private static final Gson gson = new Gson();

    private int subjectId;

    private String name, shortName;

    public static Subject fromString( String data ) {
        return gson.fromJson( data, Subject.class );
    }

    public static String toString( Subject subject ) {
        return gson.toJson( subject );
    }
}
