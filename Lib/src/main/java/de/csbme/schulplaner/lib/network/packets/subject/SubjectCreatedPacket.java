package de.csbme.schulplaner.lib.network.packets.subject;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SubjectCreatedPacket extends Packet {

    private int userId;
}
