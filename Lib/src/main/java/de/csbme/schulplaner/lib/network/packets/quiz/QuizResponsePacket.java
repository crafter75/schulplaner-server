package de.csbme.schulplaner.lib.network.packets.quiz;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Quiz;

import java.util.List;
import java.util.stream.Collectors;

public class QuizResponsePacket extends Packet {

    private List<String> quizzes;

    public QuizResponsePacket(List<Quiz> quizzes) {
        this.quizzes = quizzes.stream().map(q -> Quiz.toString(q)).collect(Collectors.toList());
    }

    public List<Quiz> getQuizzes() {
        return this.quizzes.stream().map(Quiz::fromString).collect(Collectors.toList());
    }
}
