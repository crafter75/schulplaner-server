package de.csbme.schulplaner.lib.object;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Timetable {

    private static final Gson gson = new Gson();

    private List<SchoolHour> timetable = new ArrayList<>();

    public List<SchoolHour> getHours( DayOfWeek dayOfWeek ) {
        return this.timetable.stream().filter( s -> s.getDay() == dayOfWeek ).sorted( Comparator.comparingInt( value -> value.getTimeHour().ordinal() ) ).collect( Collectors.toList() );
    }

    public static Timetable fromString( String data ) {
        return gson.fromJson( data, Timetable.class );
    }

    public static String toString( Timetable timetable ) {
        return gson.toJson( timetable );
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public static class SchoolHour {

        private int schoolHourId;

        private DayOfWeek day;

        private TimeHour timeHour;

        private int subjectId;

        private String room, teacher;

        public static SchoolHour fromString( String data ) {
            return gson.fromJson( data, SchoolHour.class );
        }

        public static String toString( SchoolHour schoolHour ) {
            return gson.toJson( schoolHour );
        }
    }

    @Getter
    @AllArgsConstructor
    public enum TimeHour {

        FIRST( 1, LocalTime.of( 7, 50, 0 ), LocalTime.of( 8, 35, 0 ) ),
        SECOND( 2, LocalTime.of( 8, 35, 0 ), LocalTime.of( 9, 20, 0 ) ),
        THIRD( 3, LocalTime.of( 9, 40, 0 ), LocalTime.of( 10, 25, 0 ) ),
        FOURTH( 4, LocalTime.of( 10, 25, 0 ), LocalTime.of( 11, 10, 0 ) ),
        FIFTH( 5, LocalTime.of( 11, 30, 0 ), LocalTime.of( 12, 15, 0 ) ),
        SIXTH( 6, LocalTime.of( 12, 15, 0 ), LocalTime.of( 13, 0, 0 ) ),
        SEVENTH( 7, LocalTime.of( 13, 15, 0 ), LocalTime.of( 14, 0, 0 ) ),
        EIGHTH( 8, LocalTime.of( 14, 0, 0 ), LocalTime.of( 14, 45, 0 ) );

        private int schoolHour;

        private LocalTime startTime, endTime;
    }
}
