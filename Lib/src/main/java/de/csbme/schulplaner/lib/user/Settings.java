package de.csbme.schulplaner.lib.user;

import lombok.Getter;

public class Settings extends AbstractData<Settings.Key> {

    @Override
    public <T> T get( Key key, Class<T> clazz ) {
        // Check if value can be cast to class
        if ( !key.getClassValue().getName().equals( clazz.getName() ) ) {
            throw new ClassCastException( "Can't cast " + key.getClassValue().getSimpleName() + " to " + clazz.getSimpleName() );
        }
        // Cast value to class
        return clazz.cast( this.getValues().getOrDefault( key.ordinal(), key.getDefaultValue() ) );
    }

    @Override
    public <V> void manipulate( Key key, V value ) {
        // Check if value is from class
        if ( !key.getClassValue().isInstance( value ) ) {
            throw new IllegalArgumentException( "Can't convert " + value.getClass().getSimpleName() + " to " + key.getClassValue().getSimpleName() );
        }
        // Store key with value
        this.getValues().put( key.ordinal(), value );
    }

    /**
     * Get the mysql data type for a java class
     *
     * @param clazz of the data type
     * @return the mysql data type as string
     */
    public static String getMySQLDataType( Class clazz ) {
        switch ( clazz.getSimpleName().toLowerCase() ) {
            case "integer":
                return "INT";
            case "long":
                return "BIGINT";
            case "string":
                return "VARCHAR(30)";
            default:
                return clazz.getSimpleName().toUpperCase();
        }
    }

    public enum Key {

        NOTIFICATIONS( "Benachrichtigungen", Boolean.class, true ),
        ANIMATION( "Animationen abspielen", Boolean.class, true );

        @Getter
        private String displayName;

        @Getter
        private Class<?> classValue;

        @Getter
        private Object defaultValue;

        Key( String displayName, Class<?> classValue, Object defaultValue ) {
            this.displayName = displayName;
            this.classValue = classValue;
            this.defaultValue = defaultValue;
        }

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }
    }
}
