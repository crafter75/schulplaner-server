package de.csbme.schulplaner.lib.network.packets.user;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Class;
import de.csbme.schulplaner.lib.object.Note;
import de.csbme.schulplaner.lib.object.Subject;
import de.csbme.schulplaner.lib.user.Settings;
import de.csbme.schulplaner.lib.user.UserGroup;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

public class UserLoginCallbackPacket extends Packet {

    @Getter
    private int id;

    @Getter
    private String username;

    @Getter
    private String mailAddress;

    @Getter
    private long registrationDate;

    @Getter
    private Settings settings;

    @Getter
    private UserGroup userGroup;

    private String classInfo;

    private List<String> notes;

    private List<String> subjects;

    public UserLoginCallbackPacket( int id, String username, String mailAddress, long registrationDate, Settings settings, UserGroup userGroup, Class classInfo, List<Note> notes, List<Subject> subjects ) {
        this.id = id;
        this.username = username;
        this.mailAddress = mailAddress;
        this.registrationDate = registrationDate;
        this.settings = settings;
        this.userGroup = userGroup;
        this.classInfo = Class.toString( classInfo );
        this.notes = notes.stream().map( n -> Note.toString( n ) ).collect( Collectors.toList() );
        this.subjects = subjects.stream().map( s -> Subject.toString( s ) ).collect( Collectors.toList() );
    }

    public List<Note> getNotes() {
        return this.notes.stream().map( Note::fromString ).collect( Collectors.toList() );
    }

    public List<Subject> getSubjects() {
        return this.subjects.stream().map( Subject::fromString ).collect( Collectors.toList() );
    }

    public Class getClassInfo() {
        return Class.fromString( this.classInfo );
    }
}
