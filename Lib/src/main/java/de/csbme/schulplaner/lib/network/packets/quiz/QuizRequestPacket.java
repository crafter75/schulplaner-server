package de.csbme.schulplaner.lib.network.packets.quiz;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class QuizRequestPacket extends Packet {

    private int userId;
}
