package de.csbme.schulplaner.lib.network.packets;

import de.csbme.schulplaner.lib.error.HTTPCodes;
import de.csbme.schulplaner.lib.network.Packet;
import lombok.Getter;

public class ErrorPacket extends Packet {

    private int httpCode;

    @Getter
    private String message;

    public ErrorPacket( HTTPCodes httpCodes, String message ) {
        this.httpCode = httpCodes.ordinal();
        this.message = message;
    }

    public HTTPCodes getErrorCode() {
        return HTTPCodes.values()[this.httpCode];
    }
}
