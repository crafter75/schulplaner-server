package de.csbme.schulplaner.lib.network.packets.timetable;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TimetableDeletePacket extends Packet {

    private int userId, schoolHourId;
}
