package de.csbme.schulplaner.lib.network.packets.note;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Note;

import java.util.List;
import java.util.stream.Collectors;

public class NoteRefreshPacket extends Packet {

    private List<String> notes;

    public NoteRefreshPacket( List<Note> notes ) {
        this.notes = notes.stream().map( n -> Note.toString( n ) ).collect( Collectors.toList() );
    }

    public List<Note> getNotes() {
        return this.notes.stream().map( Note::fromString ).collect( Collectors.toList() );
    }
}
