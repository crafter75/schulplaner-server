package de.csbme.schulplaner.lib.object;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Changelog implements Comparable<Changelog> {

    private static final Gson gson = new Gson();

    private String version, description;

    private long date;

    public static Changelog fromString( String data ) {
        return gson.fromJson( data, Changelog.class );
    }

    public static String toString( Changelog changelog ) {
        return gson.toJson( changelog );
    }

    @Override
    public int compareTo( Changelog o ) {
        return Long.compare( this.date, o.getDate() );
    }
}
