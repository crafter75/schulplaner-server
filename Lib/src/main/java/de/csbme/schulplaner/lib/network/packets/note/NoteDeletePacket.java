package de.csbme.schulplaner.lib.network.packets.note;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NoteDeletePacket extends Packet {

    private int userId, noteId;
}
