package de.csbme.schulplaner.lib.network.packets.subject;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SubjectUpdatePacket extends Packet {

    private int userId, subjectId;

    private String subjectName, subjectShortName;
}
