package de.csbme.schulplaner.lib.network.packets.timetable;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Timetable;
import lombok.Getter;

public class TimetableRefreshPacket extends Packet {

    @Getter
    private int userId;

    private String timetable;

    public TimetableRefreshPacket( int userId, Timetable timetable ) {
        this.userId = userId;
        this.timetable = Timetable.toString( timetable );
    }

    public Timetable getTimetable() {
        return Timetable.fromString( this.timetable );
    }
}
