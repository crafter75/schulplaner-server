package de.csbme.schulplaner.lib.network.packets.subject;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Subject;

import java.util.List;
import java.util.stream.Collectors;

public class SubjectRefreshPacket extends Packet {

    private List<String> subjects;

    public SubjectRefreshPacket( List<Subject> subjects ) {
        this.subjects = subjects.stream().map( s -> Subject.toString( s ) ).collect( Collectors.toList());
    }

    public List<Subject> getSubjects() {
        return this.subjects.stream().map( Subject::fromString ).collect( Collectors.toList());
    }
}
