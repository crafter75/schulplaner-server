package de.csbme.schulplaner.lib.user;

import lombok.Getter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractData<K> implements Serializable {

    @Getter
    private Map<Integer, Object> values;

    AbstractData() {
        this.values = new HashMap<>();
    }

    public abstract <V> V get( K key, Class<V> clazz );

    public abstract <V> void manipulate( K key, V value );
}
