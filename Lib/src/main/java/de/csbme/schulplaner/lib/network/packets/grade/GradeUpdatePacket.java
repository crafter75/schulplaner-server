package de.csbme.schulplaner.lib.network.packets.grade;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Grade;
import lombok.Getter;

public class GradeUpdatePacket extends Packet {

    @Getter
    private int userId;

    private String grade;

    public GradeUpdatePacket( int userId, Grade grade ) {
        this.userId = userId;
        this.grade = Grade.toString( grade );
    }

    public Grade getGrade() {
        return Grade.fromString( this.grade );
    }
}
