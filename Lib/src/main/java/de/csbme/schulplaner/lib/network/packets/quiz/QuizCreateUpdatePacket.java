package de.csbme.schulplaner.lib.network.packets.quiz;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Quiz;
import lombok.Getter;

public class QuizCreateUpdatePacket extends Packet {

    @Getter
    private int userId;

    private String quiz;

    public QuizCreateUpdatePacket(int userId, Quiz quiz) {
        this.userId = userId;
        this.quiz = Quiz.toString(quiz);
    }

    public Quiz getQuiz() {
        return Quiz.fromString(this.quiz);
    }
}
