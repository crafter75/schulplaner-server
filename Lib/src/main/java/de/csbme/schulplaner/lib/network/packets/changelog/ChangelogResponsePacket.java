package de.csbme.schulplaner.lib.network.packets.changelog;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Changelog;

import java.util.List;
import java.util.stream.Collectors;

public class ChangelogResponsePacket extends Packet {

    private List<String> changelogs;

    public ChangelogResponsePacket( List<Changelog> changelogs ) {
        this.changelogs = changelogs.stream().map( c -> Changelog.toString( c ) ).collect( Collectors.toList() );
    }

    public List<Changelog> getChangeLogs() {
        return this.changelogs.stream().map( Changelog::fromString ).collect( Collectors.toList() );
    }
}
