package de.csbme.schulplaner.lib.network.packets.schoolclass;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ClassMemberSizeRequestPacket extends Packet {

    private int classId;
}
