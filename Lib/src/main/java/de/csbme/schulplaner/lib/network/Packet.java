package de.csbme.schulplaner.lib.network;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

public abstract class Packet implements Serializable {

    @Getter
    private UUID packetId = UUID.randomUUID();

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{packetId=" + this.packetId + "}";
    }
}
