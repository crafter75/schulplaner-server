package de.csbme.schulplaner.lib.network.packets.note;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Note;
import lombok.Getter;

public class NoteReminderPacket extends Packet {

    @Getter
    private int userId;

    private String note;

    @Getter
    private boolean playSound;

    public NoteReminderPacket( int userId, Note note, boolean playSound ) {
        this.userId = userId;
        this.note = Note.toString( note );
        this.playSound = playSound;
    }

    public Note getNote() {
        return Note.fromString( this.note );
    }
}
