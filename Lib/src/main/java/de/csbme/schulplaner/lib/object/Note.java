package de.csbme.schulplaner.lib.object;

import com.google.gson.Gson;
import de.csbme.schulplaner.lib.user.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class Note {

    private static final Gson gson = new Gson();

    private int noteId;

    @Setter
    private int subjectId, category;

    private int ownerType;

    @Setter
    private String title, content;

    private long createDate, updateDate;

    @Setter
    private long expireDate;

    @Setter
    private boolean done;

    public Note( int subjectId, Category category, OwnerType ownerType, String title, String content, long expireDate, boolean done ) {
        this.noteId = -1;
        this.subjectId = subjectId;
        this.category = category.ordinal();
        this.ownerType = ownerType.ordinal();
        this.title = title;
        this.content = content;
        this.createDate = System.currentTimeMillis();
        this.updateDate = this.createDate;
        this.expireDate = expireDate;
        this.done = done;
    }

    public enum OwnerType {
        USER,
        CLASS;
    }

    public static Note fromString( String data ) {
        return gson.fromJson( data, Note.class );
    }

    public static String toString( Note note ) {
        return gson.toJson( note );
    }
}
