package de.csbme.schulplaner.lib.network.packets.note;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Note;
import lombok.Getter;

public class NoteUpdatePacket extends Packet {

    @Getter
    private int userId;

    private String note;

    public NoteUpdatePacket( int userId, Note note ) {
        this.userId = userId;
        this.note = Note.toString( note );
    }

    public Note getNote() {
        return Note.fromString( this.note );
    }
}
