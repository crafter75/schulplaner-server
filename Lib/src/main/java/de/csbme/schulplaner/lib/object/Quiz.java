package de.csbme.schulplaner.lib.object;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public class Quiz {

    private static final Gson gson = new Gson();

    private int quizId;

    @Setter
    private String name, description;

    private int createdBy;

    private long date;

    private int publicQuizId;

    @Setter
    private boolean publicQuiz;

    private List<Question> questions;

    public Quiz(String name, String description, int createdBy, boolean publicQuiz) {
        this.quizId = -1;
        this.name = name;
        this.description = description;
        this.createdBy = createdBy;
        this.date = System.currentTimeMillis();
        this.publicQuiz = publicQuiz;
        this.publicQuizId = -1;
        this.questions = new ArrayList<>();
    }

    public static Quiz fromString(String data) {
        return gson.fromJson(data, Quiz.class);
    }

    public static String toString(Quiz quiz) {
        return gson.toJson(quiz);
    }

    @Getter
    @AllArgsConstructor
    public static class Question implements Serializable {
        private int questionId;

        @Setter
        private String question, answer1, answer2, answer3, answer4;

        @Setter
        private int rightAnswer;

        public Question(String question, String answer1, String answer2, String answer3, String answer4, int rightAnswer) {
            this.questionId = -1;
            this.question = question;
            this.answer1 = answer1;
            this.answer2 = answer2;
            this.answer3 = answer3;
            this.answer4 = answer4;
            this.rightAnswer = rightAnswer;
        }
    }
}
