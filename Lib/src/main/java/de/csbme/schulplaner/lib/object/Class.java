package de.csbme.schulplaner.lib.object;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Class {

    private static final Gson gson = new Gson();

    private int classId;

    private String name, schoolName;

    private int createdById, classCode;

    public static Class fromString( String data ) {
        return gson.fromJson( data, Class.class );
    }

    public static String toString( Class classObject ) {
        return gson.toJson( classObject );
    }
}
