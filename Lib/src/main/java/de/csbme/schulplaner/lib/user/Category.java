package de.csbme.schulplaner.lib.user;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public enum Category implements Serializable {

    NOTES("Notiz"),
    PRESENTATION("Präsentation"),
    EXAM("Klausur/Prüfung"),
    TEST("Test"),
    HOMEWORK("Hausaufgabe");

    private String displayName;
}
