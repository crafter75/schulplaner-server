package de.csbme.schulplaner.lib.error;

public enum HTTPCodes {

    // Client-Error
    ERROR_400( "Bad Request" ),
    ERROR_401( "Unauthorized" ),
    ERROR_402( "Payment Required" ),
    ERROR_403( "Forbidden" ),
    ERROR_404( "Not Found" ),
    ERROR_405( "Method Not Allowed" ),
    ERROR_406( "Not Acceptable" ),
    ERROR_407( "Proxy Authentication Required" ),
    ERROR_408( "Request Timeout" ),
    ERROR_409( "Conflict" ),
    ERROR_410( "Gone" ),
    ERROR_411( "Length Required" ),
    ERROR_412( "Precondition Failed" ),
    ERROR_413( "Request Entity Too Large" ),
    ERROR_414( "URI Too Long" ),
    ERROR_415( "Unsupported Media Type" ),
    ERROR_416( "Requested range not satisfiable" ),
    ERROR_417( "Expectation Failed" ),
    ERROR_418( "I’m a teapot" ),
    ERROR_420( "Policy Not Fulfilled" ),
    ERROR_421( "Misdirected Request" ),
    ERROR_422( "Unprocessable Entity" ),
    ERROR_423( "Locked" ),
    ERROR_424( "Failed Dependency" ),
    ERROR_425( "Unordered Collection" ),
    ERROR_426( "Upgrade Required" ),
    ERROR_428( "Precondition Required" ),
    ERROR_429( "Too Many Requests" ),
    ERROR_431( "Request Header Fields Too Large" ),
    ERROR_444( "No Response" ),
    ERROR_449( "The request should be retried after doing the appropriate action" ),
    ERROR_499( "Client Closed Request" ),
    ERROR_451( "Unavailable For Legal Reasons" ),

    // Server-Error
    ERROR_500( "Internal Server Error" ),
    ERROR_501( "Not Implemented" ),
    ERROR_502( "Bad Gateway" ),
    ERROR_503( "Service Unavailable" ),
    ERROR_504( "Gateway Timeout" ),
    ERROR_505( "HTTP Version not supported" ),
    ERROR_506( "Variant Also Negotiates" ),
    ERROR_507( "Insufficient Storage" ),
    ERROR_508( "Loop Detected" ),
    ERROR_509( "Bandwidth Limit Exceeded" ),
    ERROR_510( "Not Extended" ),
    ERROR_511( "Network Authentication Required" );

    private final String message;

    HTTPCodes( String message ) {
        this.message = message;
    }

    @Override
    public String toString() {
        return this.name().split( "_" )[1] + " - " + this.message;
    }

}
