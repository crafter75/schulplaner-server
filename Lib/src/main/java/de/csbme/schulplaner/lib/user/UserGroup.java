package de.csbme.schulplaner.lib.user;

import java.io.Serializable;

public enum UserGroup implements Serializable {

    DEFAULT,
    ADMINISTRATOR;
}
