package de.csbme.schulplaner.lib.network.packets.timetable;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Timetable;
import lombok.Getter;

import java.util.Arrays;

public class TimetableUpdatePacket extends Packet {

    @Getter
    private int userId;

    private String[] schoolHours;

    public TimetableUpdatePacket( int userId, Timetable.SchoolHour[] schoolHours ) {
        this.userId = userId;
        this.schoolHours = Arrays.stream( schoolHours ).map( s -> Timetable.SchoolHour.toString( s ) ).toArray( String[]::new );
    }

    public Timetable.SchoolHour[] getSchoolHours() {
        return Arrays.stream( this.schoolHours ).map( Timetable.SchoolHour::fromString ).toArray( Timetable.SchoolHour[]::new );
    }
}
