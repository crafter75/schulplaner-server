package de.csbme.schulplaner.lib.network.packets.quiz;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Quiz;

public class QuizSearchResponsePacket extends Packet {

    private String quiz;

    public QuizSearchResponsePacket(Quiz quiz) {
        this.quiz = Quiz.toString(quiz);
    }

    public Quiz getQuiz() {
        return Quiz.fromString(this.quiz);
    }
}
