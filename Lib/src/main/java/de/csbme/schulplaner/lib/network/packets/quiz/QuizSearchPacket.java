package de.csbme.schulplaner.lib.network.packets.quiz;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class QuizSearchPacket extends Packet {

    private int userId, quizPublicId;
}
