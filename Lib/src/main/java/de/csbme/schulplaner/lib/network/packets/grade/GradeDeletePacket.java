package de.csbme.schulplaner.lib.network.packets.grade;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GradeDeletePacket extends Packet {

    private int userId;

    private int[] gradeIds;
}
