package de.csbme.schulplaner.lib.network.packets.schoolclass;

import de.csbme.schulplaner.lib.network.Packet;
import de.csbme.schulplaner.lib.object.Class;
import lombok.Getter;

public class ClassJoinedPacket extends Packet {

    @Getter
    private int userId;

    private String classInfo;

    public ClassJoinedPacket( int userId, Class classInfo ) {
        this.userId = userId;
        this.classInfo = Class.toString( classInfo );
    }

    public Class getClassInfo() {
        return Class.fromString( this.classInfo );
    }
}
