package de.csbme.schulplaner.lib.network.packets.subject;

import de.csbme.schulplaner.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SubjectCreatePacket extends Packet {

    private int userId, classId;

    private String subjectName, subjectShortName;
}
